<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePostes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postes', function (Blueprint $table) {
            $table->increments('idPoste');
            $table->integer('idBureau')->unsigned();
            $table->string('code');
            $table->string('login');
            $table->string('password');
            $table->enum('etat',['actif','inactif'])->default('inactif');
            $table->timestamp('dateCreation')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('idBureau')
                ->on('bureaux')
                ->references('idBureau')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postes');
    }
}
