<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableInspecteurs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspecteurs', function (Blueprint $table) {
            $table->increments('idInspecteur');
            $table->integer('idEtudiant')->unsigned();
            $table->integer('idBureau')->unsigned();
            $table->integer('idCandidat')->unsigned();
            $table->timestamp('dateCreation')->default(DB::raw('current_timestamp'));
            $table->foreign('idEtudiant')
                ->on('etudiants')
                ->references('idEtudiant')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('idBureau')
                ->on('bureaux')
                ->references('idBureau')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('idCandidat')
                ->on('candidats')
                ->references('idCandidat')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspecteurs');
    }
}
