<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtudiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiants', function (Blueprint $table) {
            $table->increments('idEtudiant');
            $table->integer('idClasse')->unsigned();
            $table->string('pseudo')->unique();
            $table->string('email')->unique();
            $table->string('matricule')->unique();
            $table->string('tel')->unique();
            $table->string('nom');
            $table->string('prenom');
            $table->string('password');
            $table->enum('sexe',['M','F'])->default('M');
            $table->string('photo')->nullable();
            $table->string('photoIbbLink')->nullable();
            $table->date('dateNaiss');
            $table->enum('type',['admin','etudiant','comite'])->default('etudiant');
            $table->string('codeVote')->nullable();
            $table->enum('etat',['actif','inactif'])->default('inactif');
            $table->enum('visibility',['visible','hidden'])->default('visible');
            $table->timestamp('dateCreation')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('idClasse')
                ->on('classes')
                ->references('idClasse')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiants');
    }
}
