<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableClasses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('idClasse');
            $table->integer('idFiliere')->unsigned();
            $table->integer('idNiveau')->unsigned();
            $table->string('code',10);
            $table->string('nom');
            $table->timestamp('dateCreation')->default(DB::raw('current_timestamp'));

            $table->foreign('idFiliere')
                ->on('filieres')
                ->references('idFiliere')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('idNiveau')
                ->on('niveaux')
                ->references('idNiveau')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
