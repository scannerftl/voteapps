<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableVotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('idVote');
            $table->integer('idCandidat')->unsigned();
            $table->integer('idPoste')->unsigned();
            $table->integer('idEtudiant')->unsigned();
            $table->timestamp('dateCreation')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->foreign('idEtudiant')
                ->on('etudiants')
                ->references('idEtudiant')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('idPoste')
                ->on('postes')
                ->references('idPoste')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('idCandidat')
                ->on('candidats')
                ->references('idCandidat')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
