<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCandidats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidats', function (Blueprint $table) {
            $table->increments('idCandidat');
            $table->integer('idEtudiant')->unsigned();
            $table->string('logo')->nullable();
            $table->string('logoIbbLink')->nullable();
            $table->string('partie');
            $table->string('slogan');
            $table->text('description');
            $table->timestamp('dateCreation')->default(DB::raw('current_timestamp'));
            $table->foreign('idEtudiant')
                ->on('etudiants')
                ->references('idEtudiant')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidats');
    }
}
