<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Bureau::class, function (Faker $faker) {
    return [
        'code' => 'Bureau-'.$faker->unique()->randomNumber('2'),
        'nom' => $faker->unique()->country,
    ];
});
