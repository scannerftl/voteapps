<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Candidat::class, function (Faker $faker) {
    return [
        'partie' => $faker->unique()->company,
        'slogan' => $faker->sentence(3),
        'description' => $faker->text(),
        'idEtudiant' => function(){
            return \App\Etudiant::whereType('etudiant')->get()->random()->idEtudiant;
        }
    ];
});
