<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Inspecteur::class, function (Faker $faker) {
    return [
        'idEtudiant' => function(){

            do{
                $etudiant = \Illuminate\Support\Facades\DB::select("SELECT DISTINCT E.idEtudiant, E.nom, E.prenom, classes.code as classe 
                                    FROM etudiants E
                                    JOIN classes ON E.idClasse = classes.idClasse 
                                    WHERE NOT EXISTS 
                                        (SELECT * FROM candidats C 
                                        WHERE E.idEtudiant = C.idEtudiant)
                                    AND E.type = 'etudiant'
                                    ORDER by RAND()
                                    LIMIT 1")[0];
                $etudiant = $etudiant->idEtudiant;
            } while (\App\Inspecteur::where('idEtudiant', $etudiant)->first());

            return $etudiant;
        },
        'idCandidat' => function(){
            return \App\Candidat::all()->random()->idCandidat;
        },
        'idBureau' => function(){
            return \App\Bureau::all()->random()->idBureau;
        }
    ];
});
