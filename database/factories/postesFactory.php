<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Poste::class, function (Faker $faker) {
    return [
        'code' => 'Poste-'.$faker->unique()->randomNumber(2),
        'login' => 'poste'.$faker->unique()->numberBetween(0,10),
        'password' => '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a',
        'idBureau' => function(){
            return \App\Bureau::all()->random()->idBureau;
        }
    ];
});
