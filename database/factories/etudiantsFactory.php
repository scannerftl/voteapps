<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Etudiant;
use Faker\Generator as Faker;

$factory->define(Etudiant::class, function (Faker $faker) {
    return [
        'pseudo' => $faker->unique()->userName,
        'email' => $faker->unique()->email,
        'matricule' => $faker->unique()->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'tel' => $faker->unique()->phoneNumber,
        'nom' => $faker->lastName,
        'prenom' => $faker->firstName,
        'password' => 'd033e22ae348aeb5660fc2140aec35850c4da997',
        'sexe' => $faker->randomElement(['M','F']),
        'photo' => 'source/user/uploads/p1.png',
        'dateNaiss' => $faker->date(),
        'etat' => 'inactif',
        'type' => $faker->randomElement(['etudiant','comite']),
        'visibility' => 'visible',
        'idClasse' => function(){
            return \App\Classe::all()->random()->idClasse;
        }
    ];
});
