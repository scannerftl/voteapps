-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2019 at 06:02 AM
-- Server version: 5.7.26-0ubuntu0.18.10.1
-- PHP Version: 7.2.19-0ubuntu0.18.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elecIAI`
--
CREATE DATABASE IF NOT EXISTS `elecIAI` DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;
USE `elecIAI`;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions` (
  `idAction` int(10) UNSIGNED NOT NULL,
  `idEtudiant` int(10) UNSIGNED NOT NULL,
  `cible` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCible` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`idAction`, `idEtudiant`, `cible`, `idCible`, `type`, `action`, `dateCreation`) VALUES
(1, 1, 'etudiants', 2, 'ajouter', 'KAMGAING TEKAM a cree kamdem', '2019-11-23 09:13:43'),
(2, 1, 'etudiants', 2, 'modifier', 'KAMGAING TEKAM a modifie kamdem', '2019-11-23 09:37:27'),
(3, 1, 'etudiants', 2, 'activer', 'KAMGAING TEKAM a active kamdem', '2019-11-23 09:37:56'),
(4, 1, 'etudiants', 2, 'desactiver', 'KAMGAING TEKAM a desactive kamdem', '2019-11-23 09:37:59'),
(5, 1, 'etudiants', 2, 'activer', 'KAMGAING TEKAM a active kamdem', '2019-11-23 09:38:01'),
(6, 1, 'etudiants', 2, 'generer', 'KAMGAING TEKAM a genere le code de vote de kamdem', '2019-11-23 09:38:04'),
(7, 1, 'candidats', 1, 'creer', 'KAMGAING TEKAM a cree le parti: Parti1', '2019-11-23 09:53:44'),
(8, 1, 'candidats', 2, 'creer', 'KAMGAING TEKAM a cree le parti: Partie 2', '2019-11-23 09:54:31'),
(9, 1, 'bureaux', 1, 'creer', 'KAMGAING TEKAM a cree le bureau: Bureau1', '2019-11-23 09:55:16'),
(10, 1, 'bureaux', 2, 'creer', 'KAMGAING TEKAM a cree le bureau: Bureau2', '2019-11-23 09:55:29'),
(11, 1, 'inspecteurs', 1, 'creer', 'KAMGAING TEKAM a cree l\'inspecteur: Kris', '2019-11-23 09:55:47'),
(12, 1, 'inspecteurs', 2, 'creer', 'KAMGAING TEKAM a cree l\'inspecteur: Goodwin', '2019-11-23 09:55:58'),
(13, 1, 'inspecteurs', 3, 'creer', 'KAMGAING TEKAM a cree l\'inspecteur: Wiegand', '2019-11-23 09:56:11'),
(14, 1, 'inspecteurs', 4, 'creer', 'KAMGAING TEKAM a cree l\'inspecteur: Deckow', '2019-11-23 09:56:23'),
(15, 1, 'postes', 1, 'creer', 'KAMGAING TEKAM a cree le poste Poste1', '2019-11-23 10:00:40'),
(16, 1, 'postes', 2, 'creer', 'KAMGAING TEKAM a cree le poste Poste2', '2019-11-23 10:01:12'),
(17, 1, 'etudiants', 9, 'activer', 'KAMGAING TEKAM a active Hintz', '2019-11-23 10:07:29'),
(18, 1, 'etudiants', 9, 'generer', 'KAMGAING TEKAM a genere le code de vote de Hintz', '2019-11-23 10:07:31'),
(19, 1, 'etudiants', 6, 'activer', 'KAMGAING TEKAM a active Goodwin', '2019-11-24 13:24:20'),
(20, 1, 'etudiants', 13, 'activer', 'KAMGAING TEKAM a active Wiegand', '2019-11-24 13:36:19'),
(21, 1, 'etudiants', 13, 'generer', 'KAMGAING TEKAM a genere le code de vote de Wiegand', '2019-11-24 13:36:23'),
(22, 1, 'etudiants', 7, 'supprimer', 'KAMGAING TEKAM a supprime Gleichner', '2019-11-24 13:40:42'),
(23, 1, 'etudiants', 53, 'ajouter', 'KAMGAING TEKAM a cree lskdjflskdjf', '2019-11-25 21:46:03'),
(24, 1, 'etudiants', 54, 'ajouter', 'KAMGAING TEKAM a cree lskdjflskdjf', '2019-11-25 21:47:25'),
(25, 1, 'etudiants', 55, 'ajouter', 'KAMGAING TEKAM a cree lskdjflskdjf', '2019-11-25 21:49:37'),
(26, 1, 'etudiants', 55, 'activer', 'KAMGAING TEKAM a active lskdjflskdjf', '2019-11-25 22:12:26'),
(27, 1, 'etudiants', 55, 'generer', 'KAMGAING TEKAM a genere le code de vote de lskdjflskdjf', '2019-11-25 22:12:53');

-- --------------------------------------------------------

--
-- Table structure for table `bureaux`
--

DROP TABLE IF EXISTS `bureaux`;
CREATE TABLE `bureaux` (
  `idBureau` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bureaux`
--

INSERT INTO `bureaux` (`idBureau`, `code`, `nom`, `dateCreation`) VALUES
(1, 'Bureau1', 'Bureau de la L2E', '2019-11-23 09:55:16'),
(2, 'Bureau2', 'Le bureau de la L2D', '2019-11-23 09:55:29');

-- --------------------------------------------------------

--
-- Table structure for table `candidats`
--

DROP TABLE IF EXISTS `candidats`;
CREATE TABLE `candidats` (
  `idCandidat` int(10) UNSIGNED NOT NULL,
  `idEtudiant` int(10) UNSIGNED NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logoIbbLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `partie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidats`
--

INSERT INTO `candidats` (`idCandidat`, `idEtudiant`, `logo`, `logoIbbLink`, `partie`, `slogan`, `description`, `dateCreation`) VALUES
(1, 47, 'source/user/candidats/Parti1/student-5dd9019b85f9b.jpeg', 'https://i.ibb.co/FsK975v/3669606bfd13.jpg', 'Parti1', 'iai Force', 'jlkjsdlf lskdjfl sjdflkjsdl fkjlksdjfl sjdkflsjdlf kjsdf', '2019-11-23 09:53:31'),
(2, 13, 'source/user/candidats/Partie 2/student-5dd901d35e5b3.jpeg', 'https://i.ibb.co/jL6cyxN/41290669dd2a.jpg', 'Partie 2', 'Faut manger avant de boire l\'eau', 'lkjsld flksjdl fkjsldfkjlskdjf lskjdflks jdflksjdfksjdf', '2019-11-23 09:54:27');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
CREATE TABLE `classes` (
  `idClasse` int(10) UNSIGNED NOT NULL,
  `idFiliere` int(10) UNSIGNED NOT NULL,
  `idNiveau` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`idClasse`, `idFiliere`, `idNiveau`, `code`, `nom`, `dateCreation`) VALUES
(1, 1, 1, 'L1A', 'Licence 1A', '2019-11-04 17:22:20'),
(2, 1, 1, 'L1B', 'Licence 1B', '2019-11-04 17:22:20'),
(3, 1, 1, 'L1C', 'Licence 1C', '2019-11-04 17:22:20'),
(4, 2, 1, 'L1D', 'Licence 1D', '2019-11-04 17:22:20'),
(5, 2, 1, 'L1E', 'Licence 1E', '2019-11-04 17:22:20'),
(6, 2, 1, 'L1F', 'Licence 1F', '2019-11-04 17:22:20'),
(7, 1, 2, 'L2A', 'Licence 2A', '2019-11-04 17:22:20'),
(8, 1, 2, 'L2B', 'Licence 2B', '2019-11-04 17:22:20'),
(9, 1, 2, 'L2C', 'Licence 2C', '2019-11-04 17:22:20'),
(10, 2, 2, 'L2D', 'Licence 2D', '2019-11-04 17:22:20'),
(11, 2, 2, 'L2E', 'Licence 2E', '2019-11-04 17:22:20'),
(12, 1, 3, 'GL3A', 'Génie Logiciel 3A', '2019-11-04 17:22:20'),
(13, 1, 3, 'GL3B', 'Génie Logiciel 3B', '2019-11-04 17:22:20'),
(14, 1, 3, 'GL3C', 'Génie Logiciel 3C', '2019-11-04 17:22:20'),
(15, 2, 3, 'SR3A', 'Systèmes et Réseaux 3A', '2019-11-04 17:22:20'),
(16, 2, 3, 'SR3B', 'Systèmes et Réseaux 3B', '2019-11-04 17:22:20'),
(17, 2, 3, 'SR3C', 'Systèmes et Réseaux 3C', '2019-11-04 17:22:20'),
(18, 3, 1, 'BA1 - A', 'Bachelor 1 -A ', '2019-11-04 17:22:20'),
(19, 3, 1, 'BA1 - B', 'Bachelor 1 - B', '2019-11-04 17:22:20'),
(21, 3, 2, 'BA2', 'Bachelor 2', '2019-11-04 17:22:20'),
(22, 3, 3, 'BA3', 'Bachelor 3', '2019-11-04 17:22:20');

-- --------------------------------------------------------

--
-- Table structure for table `etudiants`
--

DROP TABLE IF EXISTS `etudiants`;
CREATE TABLE `etudiants` (
  `idEtudiant` int(10) UNSIGNED NOT NULL,
  `idClasse` int(10) UNSIGNED NOT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `matricule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sexe` enum('M','F') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'M',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photoIbbLink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateNaiss` date NOT NULL,
  `type` enum('admin','etudiant','comite') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'etudiant',
  `codeVote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etat` enum('actif','inactif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactif',
  `visibility` enum('visible','hidden') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'visible',
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `etudiants`
--

INSERT INTO `etudiants` (`idEtudiant`, `idClasse`, `pseudo`, `email`, `matricule`, `tel`, `nom`, `prenom`, `password`, `sexe`, `photo`, `photoIbbLink`, `dateNaiss`, `type`, `codeVote`, `etat`, `visibility`, `dateCreation`) VALUES
(1, 16, 'MrWiltek', 'underclash94@gmail.com', '111.124.2017iai', '698154433', 'KAMGAING TEKAM', 'Steve william', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1994-05-13', 'comite', '', 'actif', 'visible', '2019-11-04 16:26:22'),
(2, 11, 'lafontaine', 'laurentfranckamdem@yahoo.fr', 'GL199901', '691882318', 'kamdem', 'franc', '8ed50507d6d42c2ef431bc0cafe5ed294640ff3a', 'M', 'source/user/images/L2E/student-5dd8fdd47d481.png', 'https://i.ibb.co/ySpZXCB/fe8be3a483c1.png', '2019-11-23', 'etudiant', 'TBs-7863', 'actif', 'visible', '2019-11-23 09:13:43'),
(3, 16, 'skyla.hamill', 'gleichner.lorenz@cremin.info', '_EL7U@6YLI.HEH', '246.416.4679', 'Kris', 'Abe', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1984-03-31', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:25'),
(4, 3, 'juana.shields', 'wbalistreri@yahoo.com', '0@4.DLW', '990-506-8390 x8777', 'Herman', 'Josefina', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1989-06-27', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:25'),
(5, 5, 'pgoldner', 'uhansen@yahoo.com', '49W@CZ.GGLW', '329-619-2843', 'Herzog', 'Estefania', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2018-07-18', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(6, 16, 'brenna.roob', 'vernice23@prohaska.info', 'I@0.JM', '1-214-848-5332 x69293', 'Goodwin', 'Liliana', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1996-02-24', 'etudiant', NULL, 'actif', 'visible', '2019-11-23 09:35:26'),
(7, 5, 'astrosin', 'nikolaus.weldon@hotmail.com', 'VT@58.FZY', '1-764-497-0413 x098', 'Gleichner', 'Ignacio', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1995-11-05', 'etudiant', NULL, 'inactif', 'hidden', '2019-11-23 09:35:26'),
(8, 9, 'leila23', 'mborer@cartwright.com', '8K1A@3FI.PLR', '750.635.4547', 'Cronin', 'Micaela', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1979-06-01', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(9, 19, 'mcclure.anthony', 'amina00@gmail.com', 'Z1@RE.DV', '(891) 631-7285', 'Hintz', 'Laney', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2000-05-17', 'etudiant', 'TBs-1685', 'actif', 'visible', '2019-11-23 09:35:26'),
(10, 11, 'amaya11', 'dibbert.priscilla@hane.com', '_@V.DMJJ', '880.852.4786 x911', 'Hudson', 'Caterina', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1971-11-20', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(11, 9, 'una01', 'mauricio11@boehm.biz', '8P@38E1.LBU', '228-416-1322', 'Harvey', 'Darrion', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2000-04-29', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(12, 8, 'craig36', 'shyann.lockman@gmail.com', 'JT@189O.LEFB', '974-503-7136 x10764', 'Boehm', 'Billy', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1983-02-14', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(13, 14, 'lincoln.okon', 'jules11@yahoo.com', 'EIIA@75.XHB', '1-306-983-1482', 'Wiegand', 'Jefferey', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2002-09-27', 'etudiant', 'TBs-7362', 'actif', 'visible', '2019-11-23 09:35:26'),
(14, 8, 'magnus.sanford', 'wmuller@yahoo.com', '7A%64S@-6XP9.OT', '886-270-5134', 'Stark', 'Aiyana', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1995-03-01', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(15, 16, 'nweimann', 'swaniawski.katelin@wuckert.com', 'C@Mi.OCJP', '934.676.8689 x55227', 'Denesik', 'Donna', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2010-04-12', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(16, 18, 'ezequiel.durgan', 'braden40@emmerich.com', 'S6@O0.RZ', '809.421.8758', 'Greenfelder', 'Diana', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1975-09-13', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(17, 6, 'makenna.dare', 'julie70@rosenbaum.com', 'P6@P.LVD', '+1 (475) 557-4991', 'Oberbrunner', 'Francesca', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1972-02-26', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(18, 9, 'blanca90', 'emile57@hotmail.com', 'ENYR@UMJ.ZODO', '1-370-635-8181 x771', 'McGlynn', 'Santiago', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2019-10-28', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(19, 5, 'mathilde00', 'lizzie.armstrong@hotmail.com', 'SK@47.JBJQ', '+15766275605', 'Hintz', 'Oceane', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2011-09-09', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(20, 11, 'fbins', 'heidenreich.malinda@reilly.com', '9@A.RWYV', '604-745-3343 x390', 'Ryan', 'Eliane', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2009-04-10', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(21, 18, 'cronin.verna', 'kurtis29@hotmail.com', '9@6K.ZCJM', '(963) 565-7056 x8163', 'Kautzer', 'Antwon', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2016-05-26', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(22, 5, 'conroy.maia', 'cameron.goodwin@hotmail.com', '%@JMSG.JE', '561-548-5068 x3226', 'Schumm', 'Blake', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1997-07-14', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(23, 13, 'carmen.heathcote', 'reinger.weldon@rodriguez.com', 'P@L.YSM', '1-681-861-6836 x03454', 'Bergnaum', 'Aidan', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1975-11-05', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(24, 3, 'tortiz', 'lkub@hotmail.com', '3@J.ZAO', '718-328-7203', 'Miller', 'Colton', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1977-05-07', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(25, 8, 'ignatius30', 'nelson.koelpin@hotmail.com', 'OME@VCW.LCV', '+1.903.519.6129', 'Bruen', 'Alexanne', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2016-04-09', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(26, 6, 'berry26', 'xlakin@yahoo.com', 'S@G.EG', '902-608-8196', 'Spinka', 'Camilla', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1978-12-12', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(27, 2, 'abogisich', 'pratke@wolf.com', '%%@VR4.YVW', '+1.498.620.6738', 'Klocko', 'Bonita', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2013-03-24', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:26'),
(28, 2, 'zemlak.cory', 'ferry.fanny@trantow.com', 'G3RZFA7%@5.QKIN', '973-539-4403', 'Kertzmann', 'Antonietta', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1977-08-11', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(29, 1, 'blanca.ryan', 'otilia.hegmann@yahoo.com', 'QD!@-.MNN', '1-692-725-3744 x654', 'Gottlieb', 'Roderick', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2014-01-27', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(30, 15, 'lohara', 'denesik.maximo@harvey.net', '4M@ZM,.LWEA', '+1 (798) 356-5898', 'Homenick', 'Amber', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1975-01-24', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(31, 12, 'cale.jerde', 'macey10@schamberger.com', '4H@TBK.KV', '860.687.8906 x6803', 'Block', 'Jace', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1998-06-18', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(32, 11, 'waters.haskell', 'donnelly.alisa@dach.com', '5B_OQ@4C1JV.ZBZ', '(349) 910-6786', 'Kunze', 'Lela', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1973-11-11', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(33, 9, 'gmante', 'schaden.ellie@abernathy.com', '9@UVV7.IXP', '821.816.8644 x54780', 'Little', 'Savanna', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2009-01-27', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(34, 6, 'kasey81', 'christa79@collins.net', 'EI%_@J49T08-.LPKD', '1-662-964-5672', 'Nikolaus', 'Jeffrey', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2004-02-04', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(35, 15, 'georgette.veum', 'gzemlak@grimes.net', 'Q7EM-_@4-.ZWZX', '1-328-374-0598', 'Christiansen', 'Daphne', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1989-12-28', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(36, 9, 'monique29', 'qharris@block.com', '1@KZVG.RHDK', '+1 (783) 618-4197', 'Langworth', 'Brendan', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2000-05-10', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(37, 9, 'hand.wendy', 'ayden33@runolfsson.com', 'S9@WF.MUWU', '+1.970.657.3893', 'Paucek', 'Reid', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2001-05-15', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(38, 6, 'ehermann', 'wisoky.ezra@williamson.com', '-EEU@Y-.PTXS', '+1-980-376-4917', 'Nicolas', 'Rylan', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2018-12-12', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(39, 4, 'robert.shanahan', 'leffler.chance@greenfelder.biz', 'V%@1P.UG', '(793) 952-9927', 'Reinger', 'Stewart', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2002-10-06', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(40, 6, 'oconner.rowland', 'roxanne.boehm@gmail.com', 'A4@R.YH', '+1-743-839-5644', 'Quigley', 'Mazie', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1992-06-09', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(41, 14, 'white.ferne', 'dpfeffer@jaskolski.com', 'M-RD@H1Z.UUS', '225-503-3035 x2103', 'Herzog', 'Daisy', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2015-02-02', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(42, 17, 'chaya17', 'cummerata.newton@rutherford.org', 'D@6V.MHCU', '(859) 412-9047 x54131', 'Weber', 'Jamison', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '2017-04-22', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(43, 3, 'pstreich', 'osinski.damian@gibson.info', 'Z3%@O8I-.LHA', '796.578.3662', 'Crona', 'Loyal', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1993-09-04', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(44, 5, 'hmcdermott', 'nkohler@gmail.com', 'IP@LK4MAV.SJRU', '612-528-5008 x2011', 'Schimmel', 'Hortense', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1970-02-14', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(45, 7, 'sven.swaniawski', 'kunze.sim@hotmail.com', '6@ADB.DJL', '(594) 412-2789 x073', 'Nader', 'Karina', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1998-08-21', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(46, 4, 'jacques20', 'emelie69@gmail.com', 'K@G.XFEF', '1-887-666-7895', 'Jacobson', 'Aubree', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1972-03-09', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(47, 2, 'slowe', 'fpaucek@waelchi.com', 'BJ2HJ8UF%@559H8.PO', '994-344-1918 x0519', 'Deckow', 'Erna', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '2010-02-28', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(48, 8, 'cwatsica', 'schinner.valerie@klocko.com', '5@R.SAJ', '+13916831418', 'Considine', 'Zoe', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1982-01-09', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(49, 1, 'raina.miller', 'otto.armstrong@koelpin.info', 'CIKETD8@0CA.TFK', '+1-391-476-8477', 'Predovic', 'Erling', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1998-06-25', 'comite', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(50, 13, 'johns.khalid', 'ksmitham@jenkins.org', 'J-SSN@FV0K.XT', '(842) 958-2790 x072', 'Cassin', 'Emma', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1986-09-09', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(51, 13, 'bwelch', 'eunice.weber@bergstrom.net', '1-B@02-S.VDBI', '1-261-620-2300 x80007', 'Stehr', 'Uriel', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'F', 'source/user/uploads/p1.png', NULL, '1987-11-20', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(52, 4, 'tommie.orn', 'lang.gretchen@will.com', '0@F.VSSI', '(785) 912-3737', 'Cruickshank', 'Rory', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', 'source/user/uploads/p1.png', NULL, '1986-11-26', 'etudiant', NULL, 'inactif', 'visible', '2019-11-23 09:35:27'),
(55, 10, 'lkjslklskjdlkjf', 'steve.wiltek25@gmail.com', 'lskdjflskjdfl sdkjf', '698154430', 'lskdjflskdjf', 'lkjsdlfkjsldkfj', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'M', NULL, NULL, '2019-10-31', 'etudiant', 'TBs-7531', 'actif', 'visible', '2019-11-25 21:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `filieres`
--

DROP TABLE IF EXISTS `filieres`;
CREATE TABLE `filieres` (
  `idFiliere` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `filieres`
--

INSERT INTO `filieres` (`idFiliere`, `code`, `nom`, `dateCreation`) VALUES
(1, 'GL', 'Génie Logiciel', '2019-11-04 17:21:11'),
(2, 'SR', 'Systèmes et Réseaux', '2019-11-04 17:21:11'),
(3, 'Ba', 'Software Engineering', '2019-11-04 17:21:11');

-- --------------------------------------------------------

--
-- Table structure for table `inspecteurs`
--

DROP TABLE IF EXISTS `inspecteurs`;
CREATE TABLE `inspecteurs` (
  `idInspecteur` int(10) UNSIGNED NOT NULL,
  `idEtudiant` int(10) UNSIGNED NOT NULL,
  `idBureau` int(10) UNSIGNED NOT NULL,
  `idCandidat` int(10) UNSIGNED NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inspecteurs`
--

INSERT INTO `inspecteurs` (`idInspecteur`, `idEtudiant`, `idBureau`, `idCandidat`, `dateCreation`) VALUES
(1, 3, 1, 1, '2019-11-23 09:55:47'),
(2, 6, 1, 2, '2019-11-23 09:55:58'),
(3, 13, 2, 1, '2019-11-23 09:56:11'),
(4, 47, 2, 2, '2019-11-23 09:56:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_10_30_155608_create_table_niveaux', 1),
(2, '2019_10_30_155629_create_table_filieres', 1),
(3, '2019_10_30_155701_create_table_classes', 1),
(4, '2019_11_04_071402_create_etudiants_table', 1),
(5, '2019_11_04_175919_create_table_bureaux', 1),
(6, '2019_11_04_180236_create_table_postes', 1),
(7, '2019_11_04_180258_create_table_notifications', 1),
(8, '2019_11_04_180321_create_table_candidats', 1),
(9, '2019_11_04_180350_create_table_inspecteurs', 1),
(10, '2019_11_04_180411_create_table_votes', 1),
(11, '2019_11_08_162124_create_table_actions', 1);

-- --------------------------------------------------------

--
-- Table structure for table `niveaux`
--

DROP TABLE IF EXISTS `niveaux`;
CREATE TABLE `niveaux` (
  `idNiveau` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreataion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `niveaux`
--

INSERT INTO `niveaux` (`idNiveau`, `code`, `nom`, `dateCreataion`) VALUES
(1, '1', 'Niveau 1', '2019-11-04 17:20:47'),
(2, '2', 'Niveau 2', '2019-11-04 17:20:47'),
(3, '3', 'Niveau 3', '2019-11-04 17:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `idNotification` int(10) UNSIGNED NOT NULL,
  `cible` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idCible` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `severite` int(11) NOT NULL,
  `etat` enum('read','unread') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `notification` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`idNotification`, `cible`, `idCible`, `type`, `severite`, `etat`, `notification`, `dateCreation`) VALUES
(1, 'postes', 1, 'essaie multiple de connexion', 2, 'read', 'Un utilisateur a raté ses identifiats 3 fois sur le poste Poste1 du bureau Bureau1', '2019-11-23 10:04:30'),
(2, 'postes', 1, 'essaie multiple de connexion', 2, 'read', 'Un utilisateur a raté ses identifiats 4 fois sur le poste Poste1 du bureau Bureau1', '2019-11-23 10:05:26'),
(3, 'postes', 1, 'essaie multiple de connexion', 2, 'read', 'Un utilisateur a raté ses identifiats 5 fois sur le poste Poste1 du bureau Bureau1', '2019-11-23 10:05:38'),
(4, 'etudiants', 9, 'voter sans code de vote', 2, 'read', 'Hintz essaie de se connecter sur le poste Poste1 du bureau Bureau1 sans avoir été activé', '2019-11-23 10:06:48'),
(5, 'etudiants', 9, 'forcer vote', 3, 'read', 'Hintz a essayé de voter deux fois sur le poste Poste1 du bureau Bureau1', '2019-11-23 10:11:42'),
(6, 'etudiants', 13, 'voter sans code de vote', 2, 'read', 'Wiegand essaie de se connecter sur le poste Poste2 du bureau Bureau2 sans avoir été activé', '2019-11-24 13:35:16');

-- --------------------------------------------------------

--
-- Table structure for table `postes`
--

DROP TABLE IF EXISTS `postes`;
CREATE TABLE `postes` (
  `idPoste` int(10) UNSIGNED NOT NULL,
  `idBureau` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` enum('actif','inactif') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'inactif',
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `postes`
--

INSERT INTO `postes` (`idPoste`, `idBureau`, `code`, `login`, `password`, `etat`, `dateCreation`) VALUES
(1, 1, 'Poste1', 'login1', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'actif', '2019-11-23 10:00:40'),
(2, 2, 'Poste2', 'login2', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', 'inactif', '2019-11-23 10:01:11');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
CREATE TABLE `votes` (
  `idVote` int(10) UNSIGNED NOT NULL,
  `idCandidat` int(10) UNSIGNED NOT NULL,
  `idPoste` int(10) UNSIGNED NOT NULL,
  `idEtudiant` int(10) UNSIGNED NOT NULL,
  `dateCreation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`idVote`, `idCandidat`, `idPoste`, `idEtudiant`, `dateCreation`) VALUES
(17, 2, 2, 55, '2019-11-25 22:46:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`idAction`);

--
-- Indexes for table `bureaux`
--
ALTER TABLE `bureaux`
  ADD PRIMARY KEY (`idBureau`);

--
-- Indexes for table `candidats`
--
ALTER TABLE `candidats`
  ADD PRIMARY KEY (`idCandidat`),
  ADD KEY `candidats_idetudiant_foreign` (`idEtudiant`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`idClasse`),
  ADD KEY `classes_idfiliere_foreign` (`idFiliere`),
  ADD KEY `classes_idniveau_foreign` (`idNiveau`);

--
-- Indexes for table `etudiants`
--
ALTER TABLE `etudiants`
  ADD PRIMARY KEY (`idEtudiant`),
  ADD UNIQUE KEY `etudiants_pseudo_unique` (`pseudo`),
  ADD UNIQUE KEY `etudiants_email_unique` (`email`),
  ADD UNIQUE KEY `etudiants_matricule_unique` (`matricule`),
  ADD UNIQUE KEY `etudiants_tel_unique` (`tel`),
  ADD KEY `etudiants_idclasse_foreign` (`idClasse`);

--
-- Indexes for table `filieres`
--
ALTER TABLE `filieres`
  ADD PRIMARY KEY (`idFiliere`);

--
-- Indexes for table `inspecteurs`
--
ALTER TABLE `inspecteurs`
  ADD PRIMARY KEY (`idInspecteur`),
  ADD KEY `inspecteurs_idetudiant_foreign` (`idEtudiant`),
  ADD KEY `inspecteurs_idbureau_foreign` (`idBureau`),
  ADD KEY `inspecteurs_idcandidat_foreign` (`idCandidat`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveaux`
--
ALTER TABLE `niveaux`
  ADD PRIMARY KEY (`idNiveau`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`idNotification`);

--
-- Indexes for table `postes`
--
ALTER TABLE `postes`
  ADD PRIMARY KEY (`idPoste`),
  ADD KEY `postes_idbureau_foreign` (`idBureau`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`idVote`),
  ADD KEY `votes_idetudiant_foreign` (`idEtudiant`),
  ADD KEY `votes_idposte_foreign` (`idPoste`),
  ADD KEY `votes_idcandidat_foreign` (`idCandidat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `idAction` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `bureaux`
--
ALTER TABLE `bureaux`
  MODIFY `idBureau` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `candidats`
--
ALTER TABLE `candidats`
  MODIFY `idCandidat` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `idClasse` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `etudiants`
--
ALTER TABLE `etudiants`
  MODIFY `idEtudiant` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `filieres`
--
ALTER TABLE `filieres`
  MODIFY `idFiliere` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inspecteurs`
--
ALTER TABLE `inspecteurs`
  MODIFY `idInspecteur` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `niveaux`
--
ALTER TABLE `niveaux`
  MODIFY `idNiveau` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `idNotification` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `postes`
--
ALTER TABLE `postes`
  MODIFY `idPoste` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `idVote` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `candidats`
--
ALTER TABLE `candidats`
  ADD CONSTRAINT `candidats_idetudiant_foreign` FOREIGN KEY (`idEtudiant`) REFERENCES `etudiants` (`idEtudiant`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `classes`
--
ALTER TABLE `classes`
  ADD CONSTRAINT `classes_idfiliere_foreign` FOREIGN KEY (`idFiliere`) REFERENCES `filieres` (`idFiliere`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `classes_idniveau_foreign` FOREIGN KEY (`idNiveau`) REFERENCES `niveaux` (`idNiveau`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `etudiants`
--
ALTER TABLE `etudiants`
  ADD CONSTRAINT `etudiants_idclasse_foreign` FOREIGN KEY (`idClasse`) REFERENCES `classes` (`idClasse`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inspecteurs`
--
ALTER TABLE `inspecteurs`
  ADD CONSTRAINT `inspecteurs_idbureau_foreign` FOREIGN KEY (`idBureau`) REFERENCES `bureaux` (`idBureau`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inspecteurs_idcandidat_foreign` FOREIGN KEY (`idCandidat`) REFERENCES `candidats` (`idCandidat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inspecteurs_idetudiant_foreign` FOREIGN KEY (`idEtudiant`) REFERENCES `etudiants` (`idEtudiant`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `postes`
--
ALTER TABLE `postes`
  ADD CONSTRAINT `postes_idbureau_foreign` FOREIGN KEY (`idBureau`) REFERENCES `bureaux` (`idBureau`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_idcandidat_foreign` FOREIGN KEY (`idCandidat`) REFERENCES `candidats` (`idCandidat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_idetudiant_foreign` FOREIGN KEY (`idEtudiant`) REFERENCES `etudiants` (`idEtudiant`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_idposte_foreign` FOREIGN KEY (`idPoste`) REFERENCES `postes` (`idPoste`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
