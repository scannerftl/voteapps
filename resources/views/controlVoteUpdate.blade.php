<div class="col-md-6 col-md-offset-3">
    <span id="{{$infosVote->idCandidat}}"></span>

    <div class="text-center card-box">
        <div class="clearfix"></div>
        <div class="member-card">
            <div class="thumb-xl member-thumb m-b-10 center-block">
                <img src="{{asset($infosVote->logo)}}" class="img-circle img-thumbnail" alt="profile-image">
                <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
            </div>

            <div class="">
                <h4 class="m-b-5"> {{\App\Etudiant::getEtudiantById($infosVote->idEtudiant)->nom}}  {{\App\Etudiant::getEtudiantById($infosVote->idEtudiant)->prenom}}</h4>
                <p class="text-danger"><i class="fa fa-thumbs-o-up"></i> {{$infosVote->nbrVote}} Votes</p>
                <p class="text-mint"><span class="text-mint"> {{$infosVote->partie}} </span></p>
                <p><span><span class="text-muted">Postule pour pour être: </span> <br> <span
                                class="text-mint">Président du comité des étudiants de l'IAI Cameroun</span> </span>
                </p>
            </div>


            <a onclick="afficherDetail({{$infosVote->idCandidat}})" class="kafe-btn kafe-btn-mint-small"><i
                        class="fa fa-user-secret" aria-hidden="true"></i> Detail du candidat</a>
        </div>
    </div>
</div>