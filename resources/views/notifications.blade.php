@extends('layouts.admin',['titre' => 'Notifications'])

@section('styles')
    <style>
        h3{
            padding: 0 0 0 33%;
            color: #188;
        }
    </style>
@endsection

@section('content')

    <!-- Info boxes -->
    <div class="row">

        <div class="col-md-12">

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="title"> Notifications </h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Cible</th>
                                <th>Type</th>
                                <th>Notification</th>
                                <th>date</th>
                            </tr>
                            </thead>
                            <tbody class="listeDesEtudiants">

                            @foreach($notifications as $n)
                                <tr>
                                    <td> {{$n->cible}}</td>
                                    <td>{{$n->type}}</td>
                                    <td>{{$n->notification}}</td>
                                    <td>{{$n->dateCreation->format('d/m/Y H:i')}}</td>
                                </tr>
                            @endforeach
                            <!-- endforeach -->
                            </tbody>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col-lg-12 -->

    </div><!-- /.row -->

    <!-- Modal -->
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Informations de l'étudiant </h4>
                </div>
                <div class="modal-body editModal">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src={{asset("source/assets/plugins/datatables/jquery.dataTables.min.js")}}></script>
    <script src={{asset("source/assets/plugins/datatables/dataTables.bootstrap.min.js")}} type="text/javascript"></script>

    {{--<script language="JavaScript">--}}
    {{--var needToConfirm = true;--}}
    {{--window.onbeforeunload = confirmExit;--}}
    {{--function confirmExit() {--}}
    {{--if (needToConfirm) {--}}
    {{--changeUserState(21);--}}
    {{--return "dont leave page please";--}}
    {{--}--}}
    {{--}--}}
    {{--</script>--}}

    <script type="text/javascript">

        $(function () {
            $('#example1').dataTable({
                "lengthMenu": [ 5, 10, 25, 50 ],
                "pagingType": "full",
                "language": {
                    "info": "de _START_ à _END_ sur _TOTAL_ étudiants",
                    "infoFiltered": " - Trié sur _MAX_ étudiants",
                    "lengthMenu":     "_MENU_ étudiants / pages",
                    "search":         "",
                    "zeroRecords":    "Aucun étudiant trouvé",
                }
            });
        })

        function modifyUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/etudiants/modifier/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (page) {

                    $('.editModal').html(page);
                    $("#editModal").modal();
                }
            });
        };


        function showUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/etudiants/afficher/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (page) {

                    $('.editModal').html(page);
                    $("#editModal").modal();
                }
            });
        };

        function genererCode(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/etudiants/genererCode/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (code) {
                    if(code == 0){
                        alert('Cet étudiant possède déja un code de vote')
                    }else{
                        $('.editModal').html("<h2 class=\"col-md-6 col-md-offset-4\">"+code+"</h2>");
                        $("#editModal").modal();
                    }
                }
            });
        };

        function changeUserState(id) {
            var token = '{{csrf_token()}}';
            $(this).id = id;
            $.ajax({
                url: '/etudiants/update/'+id,
                data: {
                    _token : token,
                    idUpdate: id
                },
                method : 'put',
                success:function (data) {
                    if(data == 'actif'){
                        $('.etat'+id).html("" +
                            "<span class=\"label label-success btn\" onclick=\"changeUserState("+id+")\" title=\"Désactiver\">Activé</span>" +
                            "<span class=\"label label-warning btn\" onclick=\"genererCode("+id+")\" title=\"Générer Code de Vote\">Générer</span>");
                    }
                    if(data == 'inactif'){
                        $('.etat'+id).html("<span class=\"label label-danger btn\" onclick=\"changeUserState("+id+")\" title=\"Activer\">Désactivé</span>");
                    }
                }
            });
        };

        function updateUserList() {
            var token = "{{csrf_token()}}";
            $.ajax({
                url: '/etudiants/list/update',
                data:{
                    _token : token
                },
                method: 'post',
                success(data){
                    $('.listeDesEtudiants').html(data);
                }
            });
        }

        function deleteUser(id){
            if(confirm("Voulez vous supprimer cet utilisateur ?")){
                var token = '{{csrf_token()}}';
                $.ajax({
                    url: '/etudiants/update/'+id,
                    data: {
                        _token : token,
                        idDelete: id
                    },
                    method : 'put',
                    success:function (data) {
                        if(data) $('#item'+id).fadeOut();
                    }
                });
            }
        };

        $(function(){
            setInterval(function(){
                updateUserList();
            },300000);
        });

    </script>

@endsection
