@extends('layouts.base')


@section('content')
    <div class="row">
        <div class="section-title" style="padding-top: 20px;">
            <h1>LES CANDIDATS AU POSTE DE PRESIDENT DU COMITE DES ETUDIANTS DE IAI-CAMEROUN</h1>
        </div>

        <div class="row">
            @foreach($candidats as $c)
                <div class="col-lg-3">
                    <span id="{{$c->idCandidat}}"></span>

                    <div class="text-center card-box">
                        <div class="clearfix"></div>
                        <div class="member-card">
                            <div class="thumb-xl member-thumb m-b-10 center-block">
                                <img src="{{asset($c->logo)}}" class="img-circle img-thumbnail" alt="profile-image">
                                <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
                            </div>

                            <div class="">
                                <h4 class="m-b-5"> {{$c->nom}} {{$c->prenom}} </h4>
                                <p class="text-mint"><span class="text-mint"> {{$c->partie}} </span></p>
                                <p> <span><span class="text-muted">Postule pour pour être: </span> <br> <span class="text-mint">Président du comité des étudiants de l'IAI Cameroun</span> </span></p>
                            </div>


                            <a onclick="afficherDetail({{$c->idCandidat}})" class="kafe-btn kafe-btn-mint-small"><i class="fa fa-user-secret" aria-hidden="true"></i> Détails</a>
                            <a onclick="vote({{$c->idCandidat}})" class="kafe-btn kafe-btn-danger-small"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> voter</a>
                        </div>
                    </div>
                </div>
            @endforeach

                <div class="modal modal-lg fade" id="voteModal" role="dialog">
                    <div class="col-md-6 col-md-offset-6">
                        <img src="{{asset('source/voter.gif')}}" class="img-circle" width="400" alt="profile-image">
                    </div>
                </div>

                <div class="modal fade" id="detailModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"> Informations du candidat </h4>
                            </div>
                            <div class=" detailModal">



                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>

                    </div>
                </div>

        </div><!-- /.row -->
@endsection

@section('scripts')
    <script type="text/javascript">
        function afficherDetail(id){
            var token = '{{csrf_token()}}';
            $.ajax({
               url : '/vote/candidat/detail/'+id,
               data: {
                   _token : token
               },
               method: 'POST',
                success:function (data) {
                    $(".detailModal").html(data);
                    $("#detailModal").modal();
                }
            });
        }

        function vote(id) {
            if(confirm("Voulez vous vraiment voter ce candidat ?")){
                var token = '{{csrf_token()}}';
                $.ajax({
                    url : '/vote/candidat/'+id,
                    method: 'POST',
                    data: {
                        _token : token
                    },
                    beforeSend:function () {
                        $('#voteModal').modal();
                    },
                    success: function (data) {
                        window.location = "/logout/etudiant"
                    }

                });
                $('#voteModal').modal();
            }
        }
    </script>
@endsection
