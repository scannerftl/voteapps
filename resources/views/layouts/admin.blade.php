<!DOCTYPE html>
<html lang="en-US" class="no-js">

<!-- Include header.php. Contains header content. -->
<head>

    <!-- ==============================================
    Title and Meta Tags
    =============================================== -->
    <meta charset="utf-8">
    <title> {{env('APP_NAME')}} | {{ $titre}}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Online Voting Platform">
    <meta name="keywords" content="<br />
<b>Notice</b>:  Undefined variable: keywords in <b>/home/fluffsco/public_html/scripts/Voting/source/admin/template/header.php</b> on line <b>15</b><br />
">
    <meta name="author" content="">

    <!-- ==============================================
    Favicons
    =============================================== -->
    <!-- ==============================================
        Favicons
        =============================================== -->
    <link rel="apple-touch-icon" sizes="57x57" href={{asset("source/assets/img/favicons/apple-touch-icon-57x57.png")}}>
    <link rel="apple-touch-icon" sizes="114x114" href={{asset("source/assets/img/favicons/apple-touch-icon-114x114.png")}}>
    <link rel="apple-touch-icon" sizes="72x72" href={{asset("source/assets/img/favicons/apple-touch-icon-72x72.png")}}>
    <link rel="apple-touch-icon" sizes="144x144" href={{asset("source/assets/img/favicons/apple-touch-icon-144x144.png")}}>
    <link rel="apple-touch-icon" sizes="60x60" href={{asset("source/assets/img/favicons/apple-touch-icon-60x60.png")}}>
    <link rel="apple-touch-icon" sizes="120x120" href={{asset("source/assets/img/favicons/apple-touch-icon-120x120.png")}}>
    <link rel="apple-touch-icon" sizes="76x76" href={{asset("source/assets/img/favicons/apple-touch-icon-76x76.png")}}>
    <link rel="apple-touch-icon" sizes="152x152" href={{asset("source/assets/img/favicons/apple-touch-icon-152x152.png")}}>
    <link rel="apple-touch-icon" sizes="180x180" href={{asset("source/assets/img/favicons/apple-touch-icon-180x180.png")}}>
    <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-96x96.png")}} sizes="96x96">
    <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-16x16.png")}} sizes="16x16">
    <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-32x32.png")}} sizes="32x32">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="msapplication-TileImage" content={{asset("source/assets/img/favicons/mstile-144x144.png")}}>
    <!-- ==============================================
    CSS
    =============================================== -->
    <!-- Style-->
    <link type="text/css" href={{asset("source/assets/css/AdminLTE/style.css")}} rel="stylesheet"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-4921714320142871",
            enable_page_level_ads: true
        });
    </script>

    @yield('styles')
</head>
<!-- Panel CSS -->
<link type="text/css" href={{asset("source/assets/css/AdminLTE/AdminLTE.min.css")}} rel="stylesheet"/>

<body class="skin-green sidebar-mini">

<!-- ==============================================
Wrapper Section
=============================================== -->
<div class="wrapper">

    <!-- Include navigation.php. Contains navigation content. -->
    <!-- ==============================================
Main Header Section
=============================================== -->
@include('partials.admin.header')
<!-- Include sidenav.php. Contains sidebar content. -->
    <!-- Left side column. contains the logo and sidebar -->

    <!-- sidebar: style can be found in sidebar.less -->
        @include('partials.admin.aside')
    <!-- /.sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1> {{$titre}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('delegue.dashboard')}}"><i class="fa fa-dashboard"></i> {{env('APP_NAME')}}</a></li>
                <li class="active"> {{$titre}}</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Info boxes -->
                @yield('content')
            <!-- /.row -->

        </section><!-- /.content -->
    </div>
</div><!-- /.content-wrapper -->


<!-- Main Footer -->
@include('partials.admin.footer')

</div>

<!-- ==============================================
Scripts
=============================================== -->

<!-- jQuery 2.1.4 -->
<script src={{asset("source/assets/js/jQuery-2.1.4.min.js")}}></script>
<!-- Bootstrap 3.3.6 JS -->
<script src={{asset("source/assets/js/bootstrap.min.js")}} type="text/javascript"></script>
<!-- AdminLTE App -->
<script src={{asset("source/assets/js/app.min.js")}} type="text/javascript"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-79656468-4', 'auto');
    ga('send', 'pageview');

</script>
<script>
    $(function (){
        setInterval(function(){
            updateNotifications();
        },10000);
    })
</script>

@yield('scripts')
</body>
</html>