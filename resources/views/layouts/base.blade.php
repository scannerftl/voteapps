<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>

    <!-- Include header.php. Contains header content. -->
    <head>
        <!-- ==============================================
        Title and Meta Tags
        =============================================== -->
        <meta charset="utf-8">
        <title>Plateforme de vote</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Online Voting Platform">
        <meta name="keywords" content="Online Voting Platform, voting ">
        <meta name="author" content="">

        <!-- ==============================================
        Favicons
        =============================================== -->
        <link rel="apple-touch-icon" sizes="57x57" href={{asset("source/assets/img/favicons/apple-touch-icon-57x57.png")}}>
        <link rel="apple-touch-icon" sizes="114x114" href={{asset("source/assets/img/favicons/apple-touch-icon-114x114.png")}}>
        <link rel="apple-touch-icon" sizes="72x72" href={{asset("source/assets/img/favicons/apple-touch-icon-72x72.png")}}>
        <link rel="apple-touch-icon" sizes="144x144" href={{asset("source/assets/img/favicons/apple-touch-icon-144x144.png")}}>
        <link rel="apple-touch-icon" sizes="60x60" href={{asset("source/assets/img/favicons/apple-touch-icon-60x60.png")}}>
        <link rel="apple-touch-icon" sizes="120x120" href={{asset("source/assets/img/favicons/apple-touch-icon-120x120.png")}}>
        <link rel="apple-touch-icon" sizes="76x76" href={{asset("source/assets/img/favicons/apple-touch-icon-76x76.png")}}>
        <link rel="apple-touch-icon" sizes="152x152" href={{asset("source/assets/img/favicons/apple-touch-icon-152x152.png")}}>
        <link rel="apple-touch-icon" sizes="180x180" href={{asset("source/assets/img/favicons/apple-touch-icon-180x180.png")}}>
        <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-96x96.png")}} sizes="96x96">
        <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-16x16.png")}} sizes="16x16">
        <link rel="icon" type="image/png" href={{asset("source/assets/img/favicons/favicon-32x32.png")}} sizes="32x32">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="msapplication-TileImage" content={{asset("source/assets/img/favicons/mstile-144x144.png")}}>
        <!-- ==============================================
            CSS
            =============================================== -->
        <!-- Style-->
        <link type="text/css" href={{asset("source/assets/css/style.css")}}  rel="stylesheet" />

        <!-- ==============================================
        Feauture Detection
        =============================================== -->
        <script src={{asset("source/assets/js/modernizr-custom.js")}}></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

        @yield('styles')
</head>
<body>

<!-- Include navigation.php. Contains navigation content. -->

<!-- ==============================================
Navigation Section
=============================================== -->
    @include('partials.header')


<!-- ==============================================
Header
=============================================== -->
<header class="header-login top-page" style="

        background: url({{asset('source/uploads/Loups_fond_ecran_dessin.jpg')}}) no-repeat center center fixed;
        background-size: cover;
        background-position: center center;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        color: #fff;
        height: 30vh;
        width: 100%;

        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;">

    <div class="container">
        <div class="content">
            <div class="row">
                <h1 class="revealOnScroll" data-animation="fadeInDown"> {{env('APP_NAME')}}</h1>
                <h2> Bureau : {{App\Bureau::find(session('idBureau'))->code ?? $infosVote->code}}</h2>
                {{session('code') ?? ""}}   
            </div><!-- /.row -->
        </div><!-- /.content -->
    </div><!-- /.container -->
</header><!-- /header -->


<!-- ==============================================
Dashboard Section
=============================================== -->
<div class="featured-users">
    <div class="container">
        @yield('content')
    </div>
</div>

<!-- Include footer.php. Contains footer content. -->


<!-- ==============================================
Footer Section
=============================================== -->
@include('partials.footer')

<a id="scrollup">Scroll</a>


<!-- ==============================================
Scripts
=============================================== -->
<script src={{asset("source/assets/js/jQuery-2.1.4.min.js")}}></script>
<script src={{asset("source/assets/js/bootstrap.min.js")}}></script>
<script src={{asset("source/assets/js/debunk.js")}}></script>

@yield('scripts')
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-79656468-4', 'auto');
    ga('send', 'pageview');

</script>


