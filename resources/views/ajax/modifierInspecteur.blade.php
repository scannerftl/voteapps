<form action="{{route('updateInspecteur',$ispt->idInspecteur)}}" method="post" enctype="multipart/form-data">

    @csrf
    {{method_field('PUT')}}

    <div class="form-group {{$errors->has('idEtudiant') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <select name="idEtudiant" id="classe" class="form-control">
                <option value=""> Inspecteur </option>
                @foreach(\App\Etudiant::listeDesEtudiants() as $etudiant)
                    <option {{($ispt->idEtudiant == $etudiant->idEtudiant ) ? 'selected' : ''}} value="{{$etudiant->idEtudiant}}"> {{$etudiant->nom}} {{$etudiant->prenom}}</option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('idEtudiant','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('idCandidat') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <select name="idCandidat" id="classe" class="form-control">
                <option value=""> Partie de l'inspecteur </option>
                @foreach(\App\Candidat::all() as $candidat)
                    <option {{($ispt->idCandidat == $candidat->idCandidat ) ? 'selected' : ''}} value="{{$candidat->idCandidat}}"> {{$candidat->partie}} </option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('idEtudiant','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('idBureau') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <select name="idBureau" id="classe" class="form-control">
                <option value=""> Bureau de l'inspecteur </option>
                @foreach(\App\Bureau::all() as $bureau)
                    <option {{($ispt->idBureau == $bureau->idBureau ) ? 'selected' : ''}} value="{{$bureau->idBureau}}"> {{$bureau->code}} -- {{$bureau->nom}} </option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('idBureau','<span class="help-block"> :message </span>') !!}
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Mettre à jour</button>
            </div>
        </div>
    </div>
</form>