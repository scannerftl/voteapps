<i class="fa fa-envelope"></i>
<span class="num">{{\App\Notification::whereEtat('unread')->count()}}</span>
<ul>
    @foreach(\App\Notification::orderBy('dateCreation','desc')->whereEtat('unread')->get() as $n)
        <li class="icon" id="notification{{$n->idNotification}}">
            @if($n->severite == 1)
                <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="icon fa fa-envelope"></span> </a>
                <span class="text label-info">{{$n->notification}}</span>
            @elseif($n->severite == 2)
                <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="fa fa-envelope"></span> </a>
                <span class="text label-warning">{{$n->notification}}</span>
            @else
                <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="fa fa-envelope"></span> </a>
                <span class="text alert-error">{{$n->notification}}</span>
            @endif
        </li>
    @endforeach
</ul>