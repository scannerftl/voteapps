<form action="{{route('updateEtudiant',$std->idEtudiant)}}" method="post" enctype="multipart/form-data">

    @csrf
    <div class="row">
        <div class="col-md-6">

            <input type="hidden" name="_method" value="PUT">

            <div class="form-group {{$errors->has('nom') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" name="nom" class="form-control" placeholder="Nom" value="{{$std->nom}}"/>
                </div>
                {!! $errors->first('nom','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group {{$errors->has('prenom') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" name="prenom" class="form-control" placeholder="Prénom" value="{{($std->prenom)}}"/>
                </div>
                {!! $errors->first('prenom','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group {{$errors->has('pseudo') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    <input type="text" name="pseudo" class="form-control" placeholder="Pseudo" value="{{($std->pseudo)}}"/>
                </div>
                {!! $errors->first('pseudo','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" name="email" class="form-control" placeholder="Adresse Email" value="{{($std->email)}}"/>
                </div>
                {!! $errors->first('email','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group {{$errors->has('tel') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input type="text" name="tel" class="form-control" placeholder="Téléphone" value="{{($std->tel)}}"/>
                </div>
                {!! $errors->first('tel','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <label class="active col-md-6"><i class="fa fa-male"></i>&nbsp&nbsp&nbsp
                            <input type="radio" {{($std->sexe == 'M') ? 'checked' : ''}} name="sexe" value="M" id="masculin" checked> Masculin
                        </label>

                        <label><i class="fa fa-female"></i>&nbsp&nbsp&nbsp
                            <input type="radio" {{($std->sexe == 'F') ? 'checked' : ''}} name="sexe" value="F" id="feminin"> Féminin
                        </label>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-md-6">

            <div class="form-group {{$errors->has('date') ? 'has-error' : ''}}">
                <label for="date" class="control-label"> date de Naissance</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input type="date" name="dateNaiss" class="form-control" value="{{($std->dateNaiss)}}"/>
                </div>
                {!! $errors->first('date','<span class="help-block"> :message </span>') !!}
            </div>

            <div class="form-group {{$errors->has('idClasse') ? 'has-error' : ''}}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                    <select name="idClasse" id="classe" class="form-control">
                        <option value=""> Salle de classe </option>
                        @foreach(\App\Classe::listeDesClasses() as $classe)
                            <option {{($classe->idClasse == $std->idClasse) ? 'selected' : ''}} value="{{$classe->idClasse}}"> {{$classe->code}} -- {{$classe->nom}}</option>
                        @endforeach
                    </select>
                </div>
                {!! $errors->first('idClasse','<span class="help-block"> :message </span>') !!}
            </div>


            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon">
                        <label class="active col-md-6"><i class="fa fa-user"></i>&nbsp
                            <input type="radio" {{($std->type == 'etudiant') ? 'checked' : ''}} name="type" value="etudiant" id="etudiant" checked> Etudiant
                        </label>

                        <label class="col-md-6"><i class="fa fa-user-secret"></i>&nbsp
                            <input type="radio" {{($std->type == 'comite') ? 'checked' : ''}} name="type" value="comite" id="admin"> Délégué
                        </label>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <label for="date" class="control-label"> matricule </label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                    <input type="text"  class="form-control" disabled value="{{($std->matricule)}}"/>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a class='btn btn-success'>Ajouter une image
                <input type="file" name="image" class="btnUpload" id="image" size="40"
                       onchange='$("#upload-file-info").html($(this).val());'>
                <input type="hidden" name="photo" id="photo"/>
            </a>
            &nbsp;
            <span class='label label-info' id="upload-file-info"></span>
        </div>
        <div class="col-md-6">

            <div class="col-md-offset-4">
                <button type="submit" class="btn btn-primary">Mettre à Jour</button>
            </div>
        </div>
    </div>
</form>