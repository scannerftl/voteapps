<form action="" method="post" enctype="multipart/form-data">

    @csrf
    <div class="row">

        <div class="col-md-6">

            <input type="hidden" name="_method" value="PUT">

            @if($ispt->sexe=='M')
            
             <div class="form-group">  
                <label for="text" class="control-label"> Nom </label>                 
                    <input type="text" name="nom" class="form-control" disabled value="M.&nbsp {{$ispt->nom}}"/>  
            </div>

             @else
                <div class="form-group">  
                <label for="text" class="control-label">Nom </label>                 
                    <input type="text" name="nom" class="form-control" disabled value="Mme/Mlle.&nbsp {{$ispt->nom}}"/>  
            </div>

            @endif

            <div class="form-group">  
                <label for="text" class="control-label"> Prénom </label>                 
                    <input type="text" name="prenom" class="form-control" disabled value="{{$ispt->prenom}}"/>
                
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> Candidat represente </label>                 
                    <input type="text" name="nomC" class="form-control" disabled value="{{$ispt->nomC}}"/>
                
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> Bureau attribue </label>                 
                    <input type="text" name="nomC" class="form-control" disabled value="{{$ispt->bureau}}"/>
                
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> E-mail </label>                 
                    <input type="text" name="email" class="form-control" disabled value="{{$ispt->email}}"/>
                
            </div>

        </div>

        <div class="col-md-6">

        <section class="col-md-offset-4">  
             <img src="{{asset($ispt->photo)}}" class="img-responsive img-rounded"/>
        </section>
         

            <div class="form-group">  
                <label for="text" class="control-label"> Classe </label>                 
                    <input type="text" name="classe" class="form-control" disabled value="{{$ispt->classe}}"/>
                
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> Matricule </label>                 
                    <input type="text" name="matricule" class="form-control" disabled value="{{$ispt->matricule}}"/>
                
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> Type </label>
                @if($ispt->type=='etudiant')
                    <input type="text" name="type" class="form-control" disabled value="Etudiant"/>
                @elseif($ispt->type=='comite')
                    <input type="text" name="type" class="form-control" disabled value="Délégué"/>  
                @else
                    <input type="text" name="type" class="form-control" disabled value="Administrateur"/>
              
                @endif
            </div>

            <div class="form-group">  
                <label for="text" class="control-label"> Téléphone </label>                 
                <input type="text" name="tel" class="form-control" disabled value="{{$ispt->tel}}"/>
                
            </div>

        </div>

</form>