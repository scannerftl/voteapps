@foreach($etudiants as $e)
    <tr id="item{{$e->idEtudiant}}">
        <td><img src="{{asset($e->photo)}}" width="50" height="30" /></td>
        <td>{{$e->nom}}</td>
        <td>{{$e->prenom}}</td>
        <td>{{\App\Classe::getClasseById($e->idClasse)->code }}</td>
        <td>{{$e->email}}</td>
        <td>{{$e->tel}}</td>
        <td class="etat{{$e->idEtudiant}}">
            @if($e->etat == 'actif')
                <span class="label label-success btn" onclick="changeUserState({{$e->idEtudiant}})" title="Désactiver">Activé</span>
                <span class="label label-warning btn" onclick="genererCode({{$e->idEtudiant}})" title="Générer Code de Vote">Générer</span>
            @else
                <span class="label label-danger btn" onclick="changeUserState({{$e->idEtudiant}})" title="Activer">Désactivé</span>
            @endif
        </td>
        <td>
            <a class="btn btn-warning btn-xs" onclick="modifyUser({{$e->idEtudiant}})" data-toggle="tooltip" id="modalBtnEdit" title="Modifier"><span class="fa fa-edit"></span></a>

            <a class="btn btn-success btn-xs" onclick="showUser({{$e->idEtudiant}})" data-toggle="tooltip" id="modalBtnShow" title="Afficher"><span class="fa fa-eye"></span></a>

            <a class="btn btn-danger btn-xs btnDel" data-toggle="tooltip" id="BtnDel" onclick="deleteUser({{$e->idEtudiant}})" title="supprimer"><span class="fa fa-times"></span></a>
        </td>
    </tr>
@endforeach