<form action="{{route('updateBureau',$br->idBureau)}}" method="post" >

    @csrf
    {{method_field('PUT')}}

    <div class="form-group {{$errors->has('code') ? 'has-error' : ''}}">
    <h4>Code du bureau</h4>
    <div class="input-group col-md-6 col-md-offset-2">         
            <span class="input-group-addon"><i class="fa fa-group"></i></span>
            <input type="text" name="code" class="form-control" value="{{$br->code}}"/>
        </div>
        {!! $errors->first('code','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('nom') ? 'has-error' : ''}}">
        <h4>Nom du bureau</h4>
        <div class="input-group col-md-6 col-md-offset-2">
            <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
            <input type="text" name="nom" class="form-control" value="{{$br->nom}}"/>
        </div>
        {!! $errors->first('nom','<span class="help-block"> :message </span>') !!}
    </div>
        <div class="col-md-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Mettre à jour</button>
            </div>
        </div>
    </div>
</form>