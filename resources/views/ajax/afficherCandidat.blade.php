<form action="{{route('updateCandidat',$cdd->idCandidat)}}" method="post" enctype="multipart/form-data">

    @csrf
    {{method_field('PUT')}}
    <div class="row">
        <div class="col-md-7">
            <div class="form-group">
                <label for="text" class="control-label"> Candidat </label>
                <input type="text" name="candidat" class="form-control" disabled value="{{$cdd->nom}} {{$cdd->prenom}}"/>
            </div>

            <div class="form-group">
                <label for="text" class="control-label"> Parti du candidat </label>
                <input type="text" name="candidat" class="form-control" disabled value="{{$cdd->partie}}"/>
            </div>
        </div>
        <div class="col-md-5">
            <div>
                <img src="{{asset($cdd->logo)}}" class="img-responsive img-rounded"/>
            </div>
        </div>
    </div>

    <div class="form-group {{$errors->has('partie') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-group"></i></span>
            <input type="text" name="partie" class="form-control" disabled placeholder="Nom du Partie" value="{{$cdd->partie}}"/>
        </div>
        {!! $errors->first('partie','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('slogan') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
            <input type="text" name="slogan" class="form-control" disabled placeholder="Slogan" value="{{($cdd->slogan)}}"/>
        </div>
        {!! $errors->first('slogan','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
        <label for="description" class="contro-label"> Description </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
            <textarea  name="description" rows="5"  class="form-control" disabled> {{($cdd->description)}} </textarea>
        </div>
        {!! $errors->first('description','<span class="help-block"> :message </span>') !!}
    </div>
</form>