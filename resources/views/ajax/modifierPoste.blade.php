<form action="{{route('updateposte',$pst->idPoste)}}" method="post" enctype="multipart/form-data">

    @csrf
    {{method_field('PUT')}}

    <div class="form-group {{$errors->has('idBureau') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <select name="idBureau" id="classe" class="form-control">
                <option value=""> Bureau de vote </option>
                @foreach(\App\Bureau::all() as $bureau)
                    <option {{($pst->idBureau == $bureau->idBureau ) ? 'selected' : ''}} value="{{$bureau->idBureau}}"> {{$bureau->code}} -- {{$bureau->nom}}</option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('idBureau','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('code') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-group"></i></span>
            <input type="text" name="code" class="form-control" placeholder=" Code du poste" value="{{$pst->code}}"/>
        </div>
        {!! $errors->first('code','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('login') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
            <input type="text" class="form-control" placeholder="Login" disabled value="{{$pst->login}}"/>
        </div>
        {!! $errors->first('login','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Mettre à jour</button>
            </div>
        </div>
    </div>
</form>