<form action="{{route('updateCandidat',$cdd->idCandidat)}}" method="post" enctype="multipart/form-data">

    @csrf
    {{method_field('PUT')}}

    <div class="form-group {{$errors->has('candidat') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-group"></i></span>
            <input type="text" name="candidat" class="form-control" placeholder="Candidat" disabled value="{{$cdd->nom}} {{$cdd->prenom}}"/>
        </div>
        {!! $errors->first('candidat','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('partie') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-group"></i></span>
            <input type="text" name="partie" class="form-control" placeholder="Nom du Partie" value="{{$cdd->partie}}"/>
        </div>
        {!! $errors->first('partie','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('slogan') ? 'has-error' : ''}}">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
            <input type="text" name="slogan" class="form-control" placeholder="Slogan" value="{{($cdd->slogan)}}"/>
        </div>
        {!! $errors->first('slogan','<span class="help-block"> :message </span>') !!}
    </div>

    <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
        <label for="description" class="contro-label"> Description </label>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
            <textarea  name="description" rows="5"  class="form-control"> {{($cdd->description)}} </textarea>
        </div>
        {!! $errors->first('description','<span class="help-block"> :message </span>') !!}
    </div>
    <div class="row">
        <div class="col-md-6">
            <a class='btn btn-success'>Ajouter une image
                <input type="file" name="image" class="btnUpload" id="image" size="40"
                       onchange='$("#upload-file-info").html($(this).val());'>
                <input type="hidden" name="photo" id="photo"/>
            </a>
            &nbsp;
            <span class='label label-info' id="upload-file-info"></span>
        </div>
        <div class="col-md-6">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary">Mettre à jour</button>
            </div>
        </div>
    </div>
</form>