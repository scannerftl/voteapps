<form action="" method="post" enctype="multipart/form-data">

    @csrf
    <div class="row">

        <div class="col-md-6">

            <input type="hidden" name="_method" value="PUT">

            @if($std->sexe=='M')

                <div class="form-group">
                    <label for="text" class="control-label"> Nom </label>
                    <input type="text" name="nom" class="form-control" disabled value="M.&nbsp {{$std->nom}}"/>
                </div>

            @else
                <div class="form-group">
                    <label for="text" class="control-label">Nom </label>
                    <input type="text" name="nom" class="form-control" disabled value="Mme/Mlle.&nbsp {{$std->nom}}"/>
                </div>

            @endif

            <div class="form-group">
                <label for="text" class="control-label"> Prénom </label>
                <input type="text" name="prenom" class="form-control" disabled value="{{$std->prenom}}"/>

            </div>

            <div class="form-group">
                <label for="text" class="control-label"> Pseudo </label>
                <input type="text" name="pseudo" class="form-control" disabled value="{{$std->pseudo}}"/>

            </div>

            <div class="form-group">
                <label for="text" class="control-label"> E-mail </label>
                <input type="text" name="email" class="form-control" disabled value="{{$std->email}}"/>

            </div>

            <div class="form-group">
                <label for="text" class="control-label"> Téléphone </label>
                <input type="text" name="tel" class="form-control" disabled value="{{$std->tel}}"/>

            </div>

        </div>

        <div class="col-md-6">

            <section class="col-md-offset-4">
                <img src="{{asset($std->photo)}}" class="img-responsive img-rounded"/>
            </section>


            <div class="form-group">
                <label for="text" class="control-label"> Classe </label>
                <input type="text" name="classe" class="form-control" disabled value="{{\App\Classe::getClasseById($std->idClasse)->code}}"/>

            </div>

            <div class="form-group">
                <label for="date" class="control-label"> Date de naissance </label>
                <input type="date" name="dateNaiss" class="form-control" disabled value="{{$std->dateNaiss}}"/>

            </div>

            <div class="form-group">
                <label for="text" class="control-label"> Matricule </label>
                <input type="text" name="matricule" class="form-control" disabled value="{{$std->matricule}}"/>

            </div>

            <div class="form-group">
                <label for="text" class="control-label"> Type </label>
                @if($std->type=='etudiant')
                    <input type="text" name="type" class="form-control" disabled value="Etudiant"/>
                @elseif($std->type=='comite')
                    <input type="text" name="type" class="form-control" disabled value="Délégué"/>
                @else
                    <input type="text" name="type" class="form-control" disabled value="Administrateur"/>

                @endif
            </div>

        </div>

</form>