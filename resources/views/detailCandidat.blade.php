<div class="section description-profile">

    <ul class="tr-list resume-info">

        <li class="personal-details">
            <div class="icon">
                <i class="fa fa-user-secret" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <span class="tr-title">Informations du président</span>
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{asset($cdd->photo)}}" class=" img-thumbnail" alt="profile-image">
                    </div>
                    <div class="col-md-8">
                        <span class="left">Nom Complet :  <b> {{$cdd->nom}} {{$cdd->prenom}} </b></span>
                        <span class="left">Classe :  <b> {{$cdd->classe}} </b></span>
                        <span class="left">Tel :  <b> {{$cdd->tel}} </b></span>
                        <span class="left">Email :  <b> {{$cdd->email}} </b></span>
                    </div>
                </div>
            </div>
        </li><!-- /.personal-details-->

        <li class="career-objective">
            <div class="icon">
                <i class="fa fa-black-tie" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <span class="tr-title">Informations du partie</span>
                <div class="row">
                    <div class="col-md-4">
                        <img src="{{asset($cdd->logo)}}" class=" img-thumbnail" alt="profile-image">
                    </div>
                    <div class="col-md-8">
                        <span class="left">Partie :  <b> {{$cdd->partie}} </b></span> <br>
                        <span class="left">Slogan :  <b> {{$cdd->slogan}} </b></span> <br>
                        <span class="left">Président :  <b> {{$cdd->nom}} {{$cdd->prenom}} </b></span> <br>
                    </div>
                </div>
            </div>
        </li><!-- /.career-objective-->

        <li class="work-history">
            <div class="icon">
                <i class="fa fa-briefcase" aria-hidden="true"></i>
            </div>
            <div class="media-body">
                <span class="tr-title"> Description </span>
                <p>
                    <span style="font-family: &quot;Abhaya Libre&quot;, serif; font-size: 18px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400;">
                        {{$cdd->description}}
                    </span><br>
                </p>
            </div>
        </li><!-- /.career-objective-->

    </ul><!-- /.ul -->

</div><!-- /.how-to -->
