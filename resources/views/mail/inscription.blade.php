
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style>
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            background: #f1f1f1;
        }
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }
        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],  /* iOS */
        .unstyle-auto-detected-links *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }
        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }
        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
            display: none !important;
        }
        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            u ~ div .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            u ~ div .email-container {
                min-width: 375px !important;
            }
        }
        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            u ~ div .email-container {
                min-width: 414px !important;
            }
        }
    </style>

    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>
        .primary{
            background: #0d0cb5;
        }
        .bg_white{
            background: #ffffff;
        }
        .bg_light{
            background: #fafafa;
        }
        .bg_black{
            background: #000000;
        }
        .bg_dark{
            background: rgba(0,0,0,.8);
        }
        .email-section{
            padding:2.5em;
        }
        /*BUTTON*/
        .btn{
            padding: 5px 15px;
            display: inline-block;
        }
        .btn.btn-primary{
            border-radius: 5px;
            background: #0d0cb5;
            color: #ffffff;
        }
        .btn.btn-white{
            border-radius: 5px;
            background: #ffffff;
            color: #000000;
        }
        .btn.btn-white-outline{
            border-radius: 5px;
            background: transparent;
            border: 1px solid #fff;
            color: #fff;
        }
        h1,h2,h3,h4,h5,h6{
            font-family: 'Poppins', sans-serif;
            color: #000000;
            margin-top: 0;
        }
        body{
            font-family: 'Poppins', sans-serif;
            font-weight: 400;
            font-size: 15px;
            line-height: 1.8;
            color: rgba(0,0,0,.4);
        }
        a{
            color: #0d0cb5;
        }
        table{
        }
        /*LOGO*/
        .logo h1{
            margin: 0;
        }
        .logo h1 a{
            color: #000000;
            font-size: 20px;
            font-weight: 700;
            text-transform: uppercase;
            font-family: 'Poppins', sans-serif;
        }
        .navigation{
            padding: 0;
        }
        .navigation li{
            list-style: none;
            display: inline-block;;
            margin-left: 5px;
            font-size: 13px;
            font-weight: 500;
        }
        .navigation li a{
            color: rgba(0,0,0,.4);
        }
        /*HERO*/
        .hero{
            position: relative;
            z-index: 0;
        }
        .hero .overlay{
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            content: '';
            width: 100%;
            background: #000000;
            z-index: -1;
            opacity: .3;
        }
        .hero .icon{
        }
        .hero .icon a{
            display: block;
            width: 60px;
            margin: 0 auto;
        }
        .hero .text{
            color: rgba(255,255,255,.8);
        }
        .hero .text h2{
            color: #ffffff;
            font-size: 30px;
            margin-bottom: 0;
        }
        /*HEADING SECTION*/
        .heading-section{
        }
        .heading-section h2{
            color: #000000;
            font-size: 20px;
            margin-top: 0;
            line-height: 1.4;
            font-weight: 700;
            text-transform: uppercase;
        }
        .heading-section .subheading{
            margin-bottom: 20px !important;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(0,0,0,.4);
            position: relative;
        }
        .heading-section .subheading::after{
            position: absolute;
            left: 0;
            right: 0;
            bottom: -10px;
            content: '';
            width: 100%;
            height: 2px;
            background: #0d0cb5;
            margin: 0 auto;
        }
        .heading-section-white{
            color: rgba(255,255,255,.8);
        }
        .heading-section-white h2{
            font-family:
            line-height: 1;
            padding-bottom: 0;
        }
        .heading-section-white h2{
            color: #ffffff;
        }
        .heading-section-white .subheading{
            margin-bottom: 0;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(255,255,255,.4);
        }
        .icon{
            text-align: center;
        }
        .icon img{
        }
        /*SERVICES*/
        .services{
            background: rgba(0,0,0,.03);
        }
        .text-services{
            padding: 10px 10px 0;
            text-align: center;
        }
        .text-services h3{
            font-size: 16px;
            font-weight: 600;
        }
        .services-list{
            padding: 0;
            margin: 0 0 20px 0;
            width: 100%;
            float: left;
        }
        .services-list img{
            float: left;
        }
        .services-list .text{
            width: calc(100% - 60px);
            float: right;
        }
        .services-list h3{
            margin-top: 0;
            margin-bottom: 0;
        }
        .services-list p{
            margin: 0;
        }
        /*BLOG*/
        .text-services .meta{
            text-transform: uppercase;
            font-size: 14px;
        }
        /*TESTIMONY*/
        .text-testimony .name{
            margin: 0;
        }
        .text-testimony .position{
            color: rgba(0,0,0,.3);
        }
        /*VIDEO*/
        .img{
            width: 100%;
            height: auto;
            position: relative;
        }
        .img .icon{
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            bottom: 0;
            margin-top: -25px;
        }
        .img .icon a{
            display: block;
            width: 60px;
            position: absolute;
            top: 0;
            left: 50%;
            margin-left: -25px;
        }
        /*COUNTER*/
        .counter{
            width: 100%;
            position: relative;
            z-index: 0;
        }
        .counter .overlay{
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            content: '';
            width: 100%;
            background: #000000;
            z-index: -1;
            opacity: .3;
        }
        .counter-text{
            text-align: center;
        }
        .counter-text .num{
            display: block;
            color: #ffffff;
            font-size: 34px;
            font-weight: 700;
        }
        .counter-text .name{
            display: block;
            color: rgba(255,255,255,.9);
            font-size: 13px;
        }
        /*FOOTER*/
        .footer{
            color: rgba(255,255,255,.5);
        }
        .footer .heading{
            color: #ffffff;
            font-size: 20px;
        }
        .footer ul{
            margin: 0;
            padding: 0;
        }
        .footer ul li{
            list-style: none;
            margin-bottom: 10px;
        }
        .footer ul li a{
            color: rgba(255,255,255,1);
        }
        @media screen and (max-width: 500px) {
            .icon{
                text-align: left;
            }
            .text-services{
                padding-left: 0;
                padding-right: 20px;
                text-align: left;
            }
        }
    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly;">
<center style="width: 100%; background-color: #f1f1f1;">
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
        <!-- BEGIN BODY -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto; background-color: white">
            <tr>
                <td valign="top" class="bg_white" style="padding: 1em 2.5em; background: #00b393">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="40%" class="logo" style="text-align: left;">
                                <h1><a href="#"> IAI Vote </a></h1>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->
            <tr>
                <td valign="middle" class="hero" style="background:#f1f1f1 url({{asset('source/admin/uploads/1517252709.jpg')}}); background-size: cover; height: 200px;">
                    <div class="overlay"></div>
                    <table>
                        <tr>
                            <td>
                                <div class="text" style="padding: 0 3em; text-align: center;">
                                    <h2> Inscription réussite !!!</h2>
                                    <p style="font-size: 1.2em">
                                        Vos informations ont été enregistré avec succès! <br>
                                        Vous êtes désormais éligible pour voter pour votre président
                                    </p>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->
            <tr>
                <td class="bg_white">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td class="bg_white email-section" style="padding:2.5em;">
                                <div class="heading-section" style="text-align: center; padding: 0 30px;">
                                    <h2>Les candidats</h2>
                                    <hr>
                                    <p> Les différents candidats cette années sont les suivant: </p>
                                </div>
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        @foreach(\App\Candidat::all() as $c)
                                        <td valign="top" width="33.333%" style="padding-top: 20px;" class="">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="icon">
                                                        <img src="{{$c->logoIbbLink}}" alt="" style="width: 60px; max-width: 600px; height: auto; margin: auto; display: block;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px 10px 0;text-align: center;">
                                                        <h3>{{$c->partie}}</h3>
                                                        <p>{{$c->slogan}}</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        @endforeach
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        <tr>
                            <td class="bg_light email-section" style="width: 100%; padding:2.5em;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="middle" width="50%">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img src="{{$data->photoIbbLink}}" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="middle" width="50%">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding-left:25px;">
                                                        <div class="heading-section">
                                                            <h2> Vos Informations </h2>
                                                            <p>
                                                                email : <span style="font-weight:bold"> {{$data->email}} </span> <br>
                                                                pseudo : <span style="font-weight:bold"> {{$data->pseudo}} </span> <br>
                                                                Mot de passe : <span style="font-weight:bold"> {{$data->password}} </span> <br>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->


                        <tr>
                            <td class="bg_light email-section" style="padding:2.5em;">
                                <div class="heading-section" style="text-align: center; padding: 0 30px;">
                                    <h2> L'équipe TheBrains</h2>
                                    <hr>
                                    <p>
                                        petit mot sur The Brains
                                    </p>
                                </div>
                                <table role="presentation" border="0" cellpadding="10" cellspacing="0" width="100%">
                                    <tr>
                                        @for($i = 0; $i < 3; $i++)
                                        <td valign="top" width="33.333%" style="padding-top: 20px;">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img src="{{asset('source/user/uploads/p1.png')}}" alt="" style="width: 70%; max-width: 600px; height: auto; margin: auto; margin-bottom: 20px; display: block;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-testimony" style="text-align: center;">
                                                        <h3 class="name">Kamgaing Tekam</h3>
                                                        <span class="position">Mr Wiltek</span>
                                                        <p>Informaticien <br> Developpeur web <br> developpeur Android </p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        @endfor
                                    </tr>
                                </table>
                                <table role="presentation" border="0" cellpadding="10" cellspacing="0" width="100%">
                                    <tr>
                                        @for($i = 0; $i < 3; $i++)
                                            <td valign="top" width="33.333%" style="padding-top: 20px;">
                                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <img src="{{asset('source/user/uploads/p1.png')}}" alt="" style="width: 70%; max-width: 600px; height: auto; margin: auto; margin-bottom: 20px; display: block;">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-testimony" style="text-align: center;">
                                                            <h3 class="name">Kamgaing Tekam</h3>
                                                            <span class="position">Mr Wiltek</span>
                                                            <p>Informaticien <br> Developpeur web <br> developpeur Android </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        @endfor
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        <tr>
                            <td class="primary email-section" style="text-align:center; background-color: #00b393; width: 100%; padding:2.5em;">
                                <div class="heading-section heading-section-white">
                                    <h2>A propos de TheBrains</h2>
                                    <hr>
                                    <p>
                                        The Brains est un groupe d'étudiants de l'Institut Africain d'Informatique qui ont pris pour initiative de mettre
                                        leurs cerveaux au service de leur établissement et aussi de la société pour résourde des problèmes dans tous les
                                        domaines des Technologies de l'Information et de la Communication
                                    </p>
                                </div>
                            </td>
                        </tr><!-- end: tr -->
                    </table>

                </td>
            </tr><!-- end:tr -->
            <!-- 1 Column Text + Button : END -->
        </table>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
            <tr>
                <td valign="top" style="background-color: black; padding:0.5em 1em;" class="footer">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td style="text-align: left;">
                                <p>&copy; {{date('Y')}} IAI Vote. All Rights Reserved</p>
                            </td>
                            <td style="text-align: right; padding-left: 5px; padding-right: 5px;">
                                <p><a href="#" style="color: rgba(255,255,255,.4);">Powered by TheBrains</a></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
                <td class="footer-bar" style="padding:35px 15px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td class="text-footer2" style="color:#85868d; font-family:'Raleway', Arial,sans-serif; font-size:8px; line-height:8px; text-align:center;"> Vous recevez ce mail parce que vous vous êtes fait enregistrer en tant qu'électeur pour le COMET 2019-2020<br> Vous receverez des messages par rapport à toutes les activités concernant le COMET 2019-2020.</td>
                        </tr>
                        </tbody></table>
                </td>
            </tr>
            </tbody></table>

    </div>
</center>

{{--d8d35ec6a51894c9f2c84a02b61cf806--}}


</body>
</html>

