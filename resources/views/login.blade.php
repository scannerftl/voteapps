@extends('layouts.login')

@section('content')
    <div class="row">

        <main class="main main-signup col-lg-12">
            <div class="col-lg-6 col-lg-offset-3 text-center">
                <div class="form-sign">
                    <form method="post">
                        <div class="form-head">
                            <h3>Authentification</h3>
                        </div><!-- /.form-head -->
                        <div class="form-body">
                        @include('flash-message');

                            @csrf

                            <div class="form-row">
                                {!! $errors->first('email','<span class="help-block"> :message </span>') !!}
                                <div class="form-controls">
                                    <input name="email" placeholder="E-mail" class="field" value="{{old('email')}}" type="email">
                                </div><!-- /.form-controls -->
                            </div><!-- /.form-row -->

                            <div class="form-row">
                                {!! $errors->first('password','<span class="help-block"> :message </span>') !!}
                                <div class="form-controls">
                                    <input name="password" placeholder="Mot de passe" class="field" type="password">
                                </div><!-- /.form-controls -->
                            </div><!-- /.form-row -->

                        </div><!-- /.form-body -->

                        <div class="form-foot">
                            <div class="form-actions">
                                <input type="hidden" name="token" value="c628fd4721e3dce01f5bf7576a4fb82d" />
                                <input value="Connexion" class="kafe-btn kafe-btn-default full-width" type="submit">
                            </div><!-- /.form-actions -->

                        </div><!-- /.form-foot -->
                    </form>

                </div><!-- /.form-sign -->
            </div><!-- /.col-lg-6 -->
        </main>

    </div><!-- /.row -->
@endsection