@extends('layouts.base')


@section('content')
    <div class="row">
        <div class="section-title" style="padding-top: 20px;">
            <h1> Page de contrôle des votes de: <b> {{\App\Etudiant::find(session('userId'))->nom}} {{\App\Etudiant::find(session('userId'))->presnom}}</b></h1>
        </div>

        <div class="row detailVote">

            <div class="col-md-6 col-md-offset-3">
                <span id="{{$infosVote->idCandidat}}"></span>

                <div class="text-center card-box">
                    <div class="clearfix"></div>
                    <div class="member-card">
                        <div class="thumb-xl member-thumb m-b-10 center-block">
                            <img src="{{asset($infosVote->logo)}}" class="img-circle img-thumbnail" alt="profile-image">
                            <i class="mdi mdi-star-circle member-star text-success" title="verified user"></i>
                        </div>

                        <div class="">
                            <h4 class="m-b-5"> {{\App\Etudiant::getEtudiantById($infosVote->idEtudiant)->nom}}  {{\App\Etudiant::getEtudiantById($infosVote->idEtudiant)->prenom}}</h4>
                            <p class="text-danger"><i class="fa fa-thumbs-o-up"></i> {{$infosVote->nbrVote}} Votes</p>
                            <p class="text-mint"><span class="text-mint"> {{$infosVote->partie}} </span></p>
                            <p><span><span class="text-muted">Postule pour pour être: </span> <br> <span
                                            class="text-mint">Président du comité des étudiants de l'IAI Cameroun</span> </span>
                            </p>
                        </div>


                        <a onclick="afficherDetail({{$infosVote->idCandidat}})" class="kafe-btn kafe-btn-mint-small"><i
                                    class="fa fa-user-secret" aria-hidden="true"></i> Detail du candidat</a>
                    </div>
                </div>
            </div><!-- /.col-lg-4 -->

        </div><!-- /.row -->


        <div class="modal fade" id="detailModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Informations du candidat </h4>
                    </div>
                    <div class=" detailModal">


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script language="JavaScript">
        var needToConfirm = true;
        window.onbeforeunload = confirmExit;
        function confirmExit() {
            if (needToConfirm) {
                return "dont leave page please";
            }
        }
    </script>

    <script>

        function updateVoteResult(){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/vote/control/update',
                data: {
                    _token: token
                },
                method: 'POST',
                success: function (data) {
                    $('.detailVote').fadeOut().fadeIn().html(data);
                }
            });
        }

        function afficherDetail(id) {
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/vote/candidat/detail/' + id,
                data: {
                    _token: token
                },
                method: 'POST',
                success: function (data) {
                    $(".detailModal").html(data);
                    $("#detailModal").modal();
                }
            });
        }

        $(function(){
            setInterval(function(){
                updateVoteResult();
            },300000);
        })

    </script>
@endsection