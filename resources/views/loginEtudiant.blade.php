@extends('layouts.login')


@section('content')
    <div class="row">

        <main class="main main-signup col-lg-12">
            <div class="col-lg-6 col-lg-offset-3 text-center">

                <div class="form-sign">
                    <form method="post">
                        <div class="form-head">
                            <h3>Authentification de l'étudiant</h3>
                        </div><!-- /.form-head -->
                        <div class="form-body">
                        @include('flash-message');
                        @csrf
                        <!-- List group -->

                            <div class="form-row">
                                <div class="form-controls">
                                    <input name="email" placeholder="E-mail" class="field" value="{{old('email')}}" type="email">
                                </div><!-- /.form-controls -->
                                {!! $errors->first('email','<span class="help-block"> :message </span>') !!}
                            </div><!-- /.form-row -->

                            <div class="form-row">
                                <div class="form-controls">
                                    <input name="codeVote" placeholder="Code de vote" class="field" value="{{old('codeVote')}}" type="text">
                                </div><!-- /.form-controls -->
                                {!! $errors->first('codeVote','<span class="help-block"> :message </span>') !!}
                            </div><!-- /.form-row -->

                            <div class="form-row">
                                <div class="form-controls">
                                    <input name="password" placeholder="Mot de passe" class="field" type="password">
                                </div><!-- /.form-controls -->
                                {!! $errors->first('codeVote','<span class="help-block"> :message </span>') !!}
                            </div><!-- /.form-row -->

                        </div><!-- /.form-body -->

                        <div class="form-foot">
                            <div class="form-actions">
                                <input value="Connexion" class="kafe-btn kafe-btn-default full-width" type="submit"><br></br>
                            </div><!-- /.form-actions -->
                        </div><!-- /.form-foot -->
                    </form>

                </div><!-- /.form-sign -->
            </div><!-- /.col-lg-6 -->
        </main>

    </div><!-- /.row -->
@endsection