<aside class="main-sidebar">
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src={{asset(\App\Etudiant::findOrFail(session('userId'))->photo)}} class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{\App\Etudiant::findOrFail(session('userId'))->nom}} {{\App\Etudiant::findOrFail(session('userId'))->prenom}}</p>
                <!-- Status -->
                <a href=""><i class="fa fa-circle text-success"></i> Comité </a>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <!-- Optionally, you can add icons to the links -->
            <li class=" {{(request()->segment(1) == '')? 'active' : ''}}">
                <a href="{{ route('delegue.dashboard')}}"><i class='fa fa-dashboard'></i> <span>Statistiques</span></a>
            </li>
            <li class=" {{(request()->segment(1) == 'notifications')? 'active' : ''}}">
                <a href="{{ route('notifications')}}"><i class='fa fa-dashboard'></i> <span>Notifications</span> <span class="badge"> {{\App\Notification::whereEtat('unread')->count()}} </span></a>
            </li>            
            <li class="treeview {{(request()->segment(1) == 'etudiants')? 'active' : ''}}">
                <a href="#"><i class='fa fa-user-plus'></i> <span>Etudiants</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{request()->is('*etudiants/ajouter') ? 'active' : ''}}">
                        <a href={{ route('ajouterEtudiant') }}>Enregistrer un étudiant</a>
                    </li>
                    <li class="{{request()->is('*etudiants/list') ? 'active' : ''}}">
                        <a href={{ route('listedesetudiants.complet') }}>Liste des étudiants</a>
                    </li>
                    <li class="{{request()->is('*etudiants/voters/*') ? 'active' : ''}}">
                        <a href={{ route('listedesetudiants') }}> Liste des voteurs </a>
                    </li>
                </ul>
            </li>
            <li class="treeview {{(request()->segment(1) == 'candidats')? 'active' : ''}}">
                <a href="#"><i class='fa fa-id-badge'></i> <span>Candidats</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{request()->is('*candidats/ajouter') ? 'active' : ''}}">
                        <a href={{ route('ajoutercandidat') }}>Enregistrer un candidat</a>
                    </li>
                    <li class="{{request()->is('*candidats/list') ? 'active' : ''}}">
                        <a href={{ route('listedescandidats') }}>Liste des candidats</a>
                    </li>
                </ul>
            </li>
            <li class="treeview {{(request()->segment(1) == 'inspecteurs')? 'active' : ''}}">
                <a href="#"><i class='fa fa-align-left'></i> <span>Inspecteurs</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{request()->is('*inspecteurs/ajouter') ? 'active' : ''}}">
                        <a href={{ route('ajouterinspecteur') }}>Enregistrer un inspecteur</a>
                    </li>
                    <li class="{{request()->is('*inspecteurs/list') ? 'active' : ''}}"><a href={{ route('listedesinspecteurs') }}>Liste des inspecteurs</a></li>
                </ul>
            </li>
            <li class="treeview {{(request()->segment(1) == 'bureaux')? 'active' : ''}}">
                <a href="#"><i class='fa fa-align-left'></i> <span>Bureaux de vote</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{request()->is('*bureaux/ajouter') ? 'active' : ''}}">
                        <a href={{ route('ajouterbureau') }}>Enregistrer un bureau de vote</a>
                    </li>
                    <li class="{{request()->is('*bureaux/list') ? 'active' : ''}}">
                        <a href={{ route('listedesbureaux') }}>Liste des bureaux de vote</a>
                    </li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa fa-id-card"></i> <span>Pdf Generetor</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href={{ route('listeEtudiantsPdf') }}>Liste des Etudiants  </a></li>
                    <li><a href={{ route('listeCandidatsPdf') }}>Liste des Candidats  </a></li>
                    <li><a href={{ route('statistiqueVotePdf') }}>Statistiques Votes  </a></li>
                    <li><a href={{ route('fichierLogPdf') }}>Fichier Logs </a></li>
                    <li><a href={{ route('listeInspecteursPdf') }}>Liste des Inspecteurs  </a></li>
                    <li><a href={{ route('listeDeleguesPdf') }}>Liste des Delegues  </a></li>
                    <li><a href={{ route('listeEtudiantsElecteuresPdf') }}>Liste des Etudiants (electeurs) </a></li>
                    <li><a href={{ route('listeBureauxPdf') }}>Liste des Bureaux de vote</a></li>
                    <li><a href={{ route('listePostesPdf') }}>Liste des Postes</a></li>
                    <li><a href={{ route('statistiqueResultatPdf') }}>Résultat finale </a></li>
                </ul>
            </li>


            <li class="treeview {{(request()->segment(1) == 'postes')? 'active' : ''}}">
                <a href="#"><i class="fa fa-id-card"></i> <span>Postes de vote</span>
                    <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{request()->is('*postes/ajouter') ? 'active' : ''}}">
                        <a href={{ route('ajouterposte') }}>Enregistrer un poste de vote</a>
                    </li>
                    <li class="{{request()->is('*postes/list') ? 'active' : ''}}">
                        <a href={{ route('listedespostes') }}>Liste des Postes de vote</a>
                    </li>
                </ul>
            </li>


        </ul><!-- /.sidebar-menu -->
    </section>
</aside>