
<style>
   


.notifications {
    width:30px;
    height:30px;
    margin: 0 50px 0 0;
    border-radius:20px;
    box-sizing:border-box;
    text-align:center;
    position:relative;
    right:1%;
    top: 9%; 
}

.notifications:hover {
    width:300px;
    right:-10%;
    height:50px;
    text-align:left;
    padding:0 15px;
    background:#012;
    transform:translateY(-0%);
    border-bottom-left-radius:0;
    border-bottom-right-radius:0;
}

.notifications:hover .fa {
    color:#fff;
}

.notifications .fa {
    color:#fff;
    line-height:30px;
    font-size:20px;
}

.notifications .num {
    position:absolute;
    width:15px;
    height:15px;
    border-radius:70%;
    background:red;
    color:#fff;
    line-height:15px;
    font-family:sans-serif;
    text-align:center;
}

.notifications:hover .num {
    position:relative;
    background:transparent;
    color:#fff;
    font-size:16px;
    top:9px;
}

.notifications:hover .num:after {
    content:' Notification(s)';
}

.notifications ul {
    position:absolute;
    left:0;
    top:50px;
    margin:0;
    width:100%;
    background:#fff;
    color: black;
    box-shadow:0 5px 15px rgba(0,0,0,.5);
    padding:20px;
    box-sizing:border-box;
    border-bottom-left-radius:30px;
    border-bottom-right-radius:30px;
    display:none;
}

.notifications:hover ul {
    display:block;
}

.notifications ul li {
    list-style:none;
    border-bottom:1px solid rgba(0,0,0,.1);
    padding:1px 0;
    display:flex;
}

.notifications ul li:last-child {
    border-bottom:none;
}

.notifications ul li .icon {
    width:24px;
    height:24px; 
    background:#012;
    border-radius:50%;
    text-align:center;
    line-height:24px;
    margin-right:15px;
}

.notifications ul li .icon .fa {
    color:#fff;
    font-size:16px;
    line-height:24px;
}

.notifications ul li .text {
    position:relative;
    font-family:sans-serif;
    top:3px;
    cursor:pointer;
}

.notifications ul li:hover .text {
    font-weight:bold;
    color:#012;
}
  
</style>



<header class="main-header">
    <a href="{{route('delegue.dashboard')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b><i class="fa fa-user"></i></b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>
       	    {{env('APP_NAME')}}</b>
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account Menu -->
                <div class="notifications navbar-nav">
                    <i class="fa fa-envelope"></i>
                    <span class="num">{{\App\Notification::whereEtat('unread')->count()}}</span>
                    <ul>
                        @foreach(\App\Notification::orderBy('dateCreation','desc')->whereEtat('unread')->get() as $n)
                        <li class="icon" id="notification{{$n->idNotification}}">
                            @if($n->severite == 1)
                            <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="icon fa fa-envelope"></span> </a>
                            <span class="text label-info">{{$n->notification}}</span>
                            @elseif($n->severite == 2)
                                <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="fa fa-envelope"></span> </a>
                                <span class="text label-warning">{{$n->notification}}</span>
                            @else
                                <a class="btn btn-success" title="marquer comme lu" onclick="read({{$n->idNotification}})"> <span class="fa fa-envelope"></span> </a>
                                <span class="text alert-error">{{$n->notification}}</span>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->

                    <img src={{asset(\App\Etudiant::findOrFail(session('userId'))->photo)}} class="user-image" alt="User Image"/>

                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                    <span class="hidden-xs"> {{\App\Etudiant::findOrFail(session('userId'))->nom}} {{\App\Etudiant::find(session('userId'))->prenom}} </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="m_2"><a href="{{route('logout')}}"><i class="fa fa-lock"></i> Deconnexion</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <script type="text/javascript">
    function read(id) {
        var token = '{{csrf_token()}}';
        $.ajax({
            url: '/notifications/read/'+id,
            data: {
                _token : token,
                idNotification: id
            },
            method : 'put',
            success:function (data) {
                $("#notification"+id).fadeOut();
                $("span.num").html(data);
                $("span.badge").html(data);
            }
        });
    };

    function updateNotifications() {
        var token = "{{csrf_token()}}";
        $.ajax({
            url: '/notifications/list/update',
            data:{
                _token : token
            },
            method: 'post',
            success(data){
                $('.notifications').html(data);
            }
        });
    };
    </script>
</header>