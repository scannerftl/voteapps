<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<style>
   
   body {
 
    background:#fff;
}

.box {
    position:absolute;
    right:1%;
    top: 9%;  
}

.notifications {
    width:30px;
    height:30px;
    background:#012;
    border-radius:20px;
    box-sizing:border-box;
    text-align:center;
}

.notifications:hover {
    width:300px;
    height:50px;
    text-align:left;
    padding:0 15px;
    background:#012;
    transform:translateY(-100%);
    border-bottom-left-radius:0;
    border-bottom-right-radius:0;
}

.notifications:hover .fa {
    color:#fff;
}

.notifications .fa {
    color:fff;
    line-height:30px;
    font-size:20px;
}

.notifications .num {
    position:absolute;
    top:0;
    right:-5px;
    width:15px;
    height:15px;
    border-radius:70%;
    background: red;
    color:fff;
    line-height:15px;
    font-family:sans-serif;
    text-align:center;
}

.notifications:hover .num {
    position:relative;
    background:transparent;
    color:#fff;
    font-size:17px;
    top:-2px;
}

.notifications:hover .num:after {
    content:' Notification(s)';
}

ul {
    position:absolute;
    left:0;
    top:50px;
    margin:0;
    width:100%;
    background:#fff;
    box-shadow:0 5px 15px rgba(0,0,0,.5);
    padding:20px;
    box-sizing:border-box;
    border-bottom-left-radius:30px;
    border-bottom-right-radius:30px;
    display:none;
}

.notifications:hover ul {
    display:block;
}

ul li {
    list-style:none;
    border-bottom:1px solid rgba(0,0,0,.1);
    padding:8px 0;
    display:flex;
}

ul li:last-child {
    border-bottom:none;
}

ul li .icon {
    width:24px;
    height:24px; 
    background:#012;
    border-radius:50%;
    text-align:center;
    line-height:24px;
    margin-right:15px;
}

ul li .icon .fa {
    color:#012;
    font-size:16px;
    line-height:24px;
}

ul li .text {
    position:relative;
    font-family:sans-serif;
    top:3px;
    cursor:pointer;
}

ul li:hover .text {
    font-weight:bold;
    color:#012;
}
  
</style>

<div class="container">
	<div class="row">
		<div class="box">
            <div class="notifications">
                <i class="fa fa-envelope"></i>
                <span class="num">4</span>
                <ul>
                    <li class="icon">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text">Someone Like Your Post</span>
                    </li>

                    <li class="icon">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text">Someone Like Your Photo</span>
                    </li>

                    <li class="icon">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text">Someone Dislike Your Post</span>
                    </li>

                    <li class="icon">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text">Someone Comment on Your Post</span>
                    </li>

                    <li class="icon">
                        <span class="icon"><i class="fa fa-envelope"></i></span>
                        <span class="text">Y'a un voleur dans la salle</span>
                    </li>
                </ul>
            </div>
        </div>
	</div>
</div>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">