<header class="tr-header">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">{{env('APP_NAME')}}</a>
            </div><!-- /.navbar-header -->

        </div><!-- /.container -->
    </nav><!-- /.navbar -->
</header><!-- Page Header -->
