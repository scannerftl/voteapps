    @extends('layouts.admin',['titre' => 'Tableau de bord'])

    @section('content')

    <style>
      .row{
          padding: 4.2em 0 0 0;
      }
    </style>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('listedescandidats')}}">
                        <span class="info-box-icon bg-aqua">
                            <i class="fa fa-id-badge"></i>
                        </span>
                    </a>
                    <div class="info-box-content">
                        <span class="info-box-text">Candidats</span>
                        <span class="info-box-number">
                    {{\App\Candidat::all()->count()}}
                    </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('listedesinspecteurs')}}">
                        <span class="info-box-icon bg-aqua">
                            <i class="fa fa-id-badge"></i>
                        </span>
                    </a>
                    <div class="info-box-content">
                        <span class="info-box-text">Inspecteurs</span>
                        <span class="info-box-number">
                    {{\App\Inspecteur::all()->count()}}
                    </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('listedesetudiants')}}">
                        <span class="info-box-icon bg-aqua">
                            <i class="fa fa-pie-chart"></i>
                        </span>
                    </a>
                    <div class="info-box-content">
                        <span class="info-box-text">Voteurs</span>
                        <span class="info-box-number">
                    {{count(\App\Etudiant::listeDesEtudiantsEligible())}}
                    </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <a href="{{route('listedesetudiants.complet')}}">
                        <span class="info-box-icon bg-aqua">
                            <i class="fa fa-users"></i>
                        </span>
                    </a>
                    <div class="info-box-content">
                        <span class="info-box-text">Etudiants</span>
                        <span class="info-box-number">
                    {{\App\Etudiant::listeDesEtudiants()->count()}}
                    </span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.col -->

        </div>
    @endsection