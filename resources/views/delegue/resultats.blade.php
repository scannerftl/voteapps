    @extends('layouts.admin',['titre' => 'Résultats'])

    @section('styles')
        <link href="source/css/bootstrap-responsivemin.css" rel="stylesheet">
    @endsection
    @section('content')
        <div class="row">
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="title"> Résultat Finaux: </h3>
                        <span class="btn btn-info pull-right"> Imprimer </span>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table example1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>partie</th>
                                    <th>Votes </th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody class="listeDesEtudiants">

                                @foreach(\App\Vote::getResult() as $e)
                                    <tr>
                                        <td>{{$e->partie}}</td>
                                        <td>{{$e->votes}}</td>
                                        <td> {{$e->pourcentage}}</td>
                                    </tr>
                                @endforeach
                                <!-- endforeach -->
                                </tbody>

                            </table>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
        <div class="row">
            @foreach(\App\Bureau::all() as $b)
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="title"> Résultat du bureau:  {{$b->code}} </h3>
                        <span class="btn btn-info pull-right"> Imprimer </span>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table example1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>partie</th>
                                    <th>Votes </th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody class="listeDesEtudiants">

                                @foreach(\App\Vote::getResultsbyBureau($b->idBureau) as $e)
                                    <tr id="item{{$e->bureau}}">
                                        <td>{{$e->partie}}</td>
                                        <td>{{$e->votes}}</td>
                                        <td> {{$e->pourcentage}}</td>
                                    </tr>
                                @endforeach
                                <!-- endforeach -->
                                </tbody>

                            </table>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
            @endforeach

        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="title"> Résultat Des Votes: </h3>
                        <span class="btn btn-info pull-right"> Imprimer </span>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table example1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>code</th>
                                    <th>Parti </th>
                                    <th> Bureau </th>
                                    <th> Poste </th>
                                    <th> date </th>
                                </tr>
                                </thead>
                                <tbody class="listeDesEtudiants">

                                @foreach(\App\Vote::getEtudiantsResult() as $e)
                                    <tr>
                                        <td>{{$e->codeVote}}</td>
                                        <td>{{$e->partie}}</td>
                                        <td>{{$e->bureau}}</td>
                                        <td>{{$e->poste}}</td>
                                        <td> {{$e->date}}</td>
                                    </tr>
                                @endforeach
                                <!-- endforeach -->
                                </tbody>

                            </table>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="title"> Etudiants n'ayant pas voté: </h3>
                        <span class="btn btn-info pull-right"> Imprimer </span>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example1" class="table example1 table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Nom Complet</th>
                                    <th>email </th>
                                    <th> Téléphone </th>
                                    <th> classe </th>
                                </tr>
                                </thead>
                                <tbody class="listeDesEtudiants">

                                @foreach(\App\Vote::getAbsentStudent() as $e)
                                    <tr>
                                        <td>{{$e->nom}}  {{$e->prenom}}</td>
                                        <td>{{$e->email}}</td>
                                        <td>{{$e->tel}}</td>
                                        <td>{{$e->classe}}</td>
                                    </tr>
                                @endforeach
                                <!-- endforeach -->
                                </tbody>

                            </table>
                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    @endsection
    @section('scripts')
        <script src={{asset("source/assets/plugins/datatables/jquery.dataTables.min.js")}}></script>
        <script src={{asset("source/assets/plugins/datatables/dataTables.bootstrap.min.js")}} type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('.example1').dataTable({
                    "lengthMenu": [ 5, 10, 25, 50 ],
                    "pagingType": "full",
                    "language": {
                        "info": "de _START_ à _END_ sur _TOTAL_ étudiants",
                        "infoFiltered": " - Trié sur _MAX_ étudiants",
                        "lengthMenu":     "_MENU_ étudiants / pages",
                        "search":         "",
                        "zeroRecords":    "Aucun étudiant trouvé",
                    }
                });
            })
        </script>



    @endsection