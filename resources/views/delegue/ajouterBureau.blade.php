@extends('layouts.admin',['titre' => 'Gestion des bureaux'])

@section('styles')

    <style>
        h3{
            padding: 0 0 0 33%;
            color: #78a;
        }
    </style>
    
@endsection


@section('content')

    <div class="row">

        <div class="col-lg-12">

            <!-- Input addon -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="title">Enregistrement des bureaux de vote</h3>
                </div>
                <div class="box-body">
                    <br>
                    <form action="" method="post" class="col-md-4 col-md-offset-4" enctype="multipart/form-data">
                        @include('flash-message')

                        @csrf

                                <div class="form-group {{$errors->has('code') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                        <input type="text" name="code" class="form-control" placeholder="Code du bureau" value="{{old('code')}}"/>
                                    </div>
                                    {!! $errors->first('code','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('nom') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
                                        <input type="text" name="nom" class="form-control" placeholder="Nom du bureau" value="{{old('nom')}}"/>
                                    </div>
                                    {!! $errors->first('nom','<span class="help-block"> :message </span>') !!}
                                </div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->


@endsection