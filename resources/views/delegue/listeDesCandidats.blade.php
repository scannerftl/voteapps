@extends('layouts.admin',['titre' => 'Gestion des Candidats'])

@section('styles')
    <style>
        .btnUpload {
            position:absolute;
            z-index:2;
            top:0;
            left:0;
            filter: alpha(opacity=0);
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            opacity:0;
            background-color:transparent;
            color:transparent;
        }

        h3{
            padding: 0 0 0 33%;
            color: #169;
        }
    </style>
@endsection

@section('content')

    <!-- Info boxes -->
    <div class="row">

        <div class="col-md-12">

            <div class="box box-info">
                @include('flash-message')

                <div class="box-header">
                    <h3 class="title"> Liste des Candidats </h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>logo</th>
                                <th>Parti</th>
                                <th>President</th>
                                <th>Slogan</th>
                                <th>E-mail</th>
                                <th>Téléphone</th>
                                <th>classe</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($candidats as $c)
                                <tr id="item{{$c->idCandidat}}">
                                    <td><img src="{{asset($c->logo)}}" width="50" height="30" /></td>
                                    <td>{{$c->partie}}</td>
                                    <td>{{$c->nom}} {{$c->prenom}}</td>
                                    <td>{{$c->slogan}}</td>
                                    <td>{{$c->email}}</td>
                                    <td>{{$c->tel}}</td>
                                    <td> {{$c->classe}} </td>
                                    <td>
                                        <a class="btn btn-warning btn-xs" onclick="modifyUser({{$c->idCandidat}})" data-toggle="tooltip" id="modalBtnEdit" title="Modifier"><span class="fa fa-edit"></span></a>

                                        <a class="btn btn-success btn-xs" onclick="showUser({{$c->idCandidat}})" data-toggle="tooltip" id="modalBtnShow" title="Afficher"><span class="fa fa-eye"></span></a>

                                        <a class="btn btn-danger btn-xs btnDel" data-toggle="tooltip" id="BtnDel" onclick="deleteUser({{$c->idCandidat}})" title="supprimer"><span class="fa fa-times"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            <!-- endforeach -->
                            </tbody>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col-lg-12 -->

    </div><!-- /.row -->

    <!-- Modal -->
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Informations du candidat </h4>
                </div>
                <div class="modal-body editModal">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('scripts')
    <script src={{asset("source/assets/plugins/datatables/jquery.dataTables.min.js")}}></script>
    <script src={{asset("source/assets/plugins/datatables/dataTables.bootstrap.min.js")}} type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            $('#example1').dataTable();
        })

        function modifyUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/candidats/modifier/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (page) {

                    $('.editModal').html(page);
                    $("#editModal").modal();
                }
            });
        };

        function showUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/candidats/afficher/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (page) {

                    $('.editModal').html(page);
                    $("#editModal").modal();
                }
            });
        };


        function deleteUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/candidats/update/'+id,
                data: {
                    _token : token,
                    idDelete: id
                },
                method : 'put',
                success:function (data) {
                    if(data) $('#item'+id).fadeOut();
                }
            });
        };


    </script>

@endsection
