@extends('layouts.admin',['titre' => 'Gestion des bureaux'])

@section('styles')

    <style>
        h3{
            padding: 0 0 0 33%;
            color: #78a;
        }
    </style>
    
@endsection

@section('content')

    <!-- Info boxes -->
    <div class="row">

        <div class="col-md-12">

            <div class="box box-info">
                @include('flash-message')
                <div class="box-header">
                    <h3 class="title"> Liste des bureaux de vote</h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="table-responsive col-md-8 col-md-offset-2">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Code</th>
                                <th>Nom</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bureaux as $b)
                                <tr id="item{{$b->idBureau}}">
                                    <td>{{$b->idBureau}}</td>
                                    <td>{{$b->code}}</td>
                                    <td> {{$b->nom}}</td>
                                    <td>
                                        <a class="btn btn-warning btn-xs" onclick="modifierBureau({{$b->idBureau}})" data-toggle="tooltip" id="modalBtnEdit" title="Modifier"><span class="fa fa-edit"></span></a>

                                        <a class="btn btn-danger btn-xs btnDel" data-toggle="tooltip" id="BtnDel" onclick="deleteBureau({{$b->idBureau}})" title="supprimer"><span class="fa fa-times"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            <!-- endforeach -->
                            </tbody>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col-lg-12 -->

    </div><!-- /.row -->

    <!-- Modal -->
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Informations du bureau de vote </h4>
                </div>
                <div class="modal-body editModal">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('scripts')
    <script src={{asset("source/assets/plugins/datatables/jquery.dataTables.min.js")}}></script>
    <script src={{asset("source/assets/plugins/datatables/dataTables.bootstrap.min.js")}} type="text/javascript"></script>
    <script>
        $(function () {
            $('#example1').dataTable();
        })
    </script>
    <script type="text/javascript">

        function modifierBureau(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/bureaux/modifier/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (data) {
                    $('.editModal').html(data);
                    $("#editModal").modal();
                }
            });
        };

        function afficherBureau(id) {
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/bureaux/afficher/'+id,
                data: {
                    _token: token
                },
                method: 'post',
                success: function(data) {
                    $('.editModal').html(data);
                    $("#editModal").modal();
                }
            });
        };


        function deleteBureau(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/bureaux/update/'+id,
                data: {
                    _token : token,
                    idDelete: id
                },
                method : 'put',
                success:function (data) {
                    if(data) $('#item'+id).fadeOut();
                }
            });
        };

    </script>

@endsection
