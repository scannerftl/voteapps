@extends('layouts.admin',['titre' => 'Gestion des Etudiants'])

@section('styles')
    <style>
        .btnUpload {
            position:absolute;
            z-index:2;
            top:0;
            left:0;
            filter: alpha(opacity=0);
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            opacity:0;
            background-color:transparent;
            color:transparent;
        }

        h3{
            padding: 0 0 0 31%;
            color: #188;
        }

    </style>
@endsection
@section('content')

    <div class="row">

        <div class="col-lg-12">

            <!-- Input addon -->
            <div class="box box-info">
                @include('flash-message')

                <div class="box-header">
                    <h3 class="title">Enregistrement des étudiants</h3>
                </div>
                <div class="box-body">
                    <br>
                    <form action="" method="post" enctype="multipart/form-data">

                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{$errors->has('nom') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="nom" class="form-control" placeholder="Nom" value="{{old('nom')}}"/>
                                    </div>
                                    {!! $errors->first('nom','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('prenom') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="prenom" class="form-control" placeholder="Prénom" value="{{old('prenom')}}"/>
                                    </div>
                                    {!! $errors->first('prenom','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('pseudo') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="text" name="pseudo" class="form-control" placeholder="Pseudo" value="{{old('pseudo')}}"/>
                                    </div>
                                    {!! $errors->first('pseudo','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                        <input type="text" name="email" class="form-control" placeholder="Adresse Email" value="{{old('email')}}"/>
                                    </div>
                                    {!! $errors->first('email','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('tel') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                        <input type="text" name="tel" class="form-control" placeholder="Téléphone" value="{{old('tel')}}"/>
                                    </div>
                                    {!! $errors->first('tel','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('matricule') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-id-badge"></i></span>
                                        <input type="text" name="matricule" class="form-control" placeholder="Matricule" value="{{old('matricule')}}"/>
                                    </div>
                                    {!! $errors->first('matricule','<span class="help-block"> :message </span>') !!}
                                </div>
                            </div>


                            <div class="col-md-6">

                                <div class="form-group {{$errors->has('dateNaiss') ? 'has-error' : ''}}">
                                    <label for="dateNaiss" class="control-label"> date de Naissance</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="date" name="dateNaiss" class="form-control" value="{{old('dateNaiss')}}"/>
                                    </div>
                                    {!! $errors->first('dateNaiss','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <label class="active col-md-6"><i class="fa fa-male"></i>
                                                <input type="radio" name="sexe" value="M" id="masculin" checked> Masculin
                                            </label>

                                            <label><i class="fa fa-female"></i>
                                                <input type="radio" name="sexe" value="F" id="feminin"> Féminin
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('idClasse') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                        <select name="idClasse" id="classe" class="form-control">
                                            <option value=""> Salle de classe </option>
                                            @foreach(\App\Classe::listeDesClasses() as $classe)
                                                <option {{(old('idClasse') == $classe->idClasse ) ? 'selected' : ''}} value="{{ $classe->idClasse}}"> {{$classe->code}} -- {{$classe->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {!! $errors->first('idClasse','<span class="help-block"> :message </span>') !!}
                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <label class="active col-md-6"><i class="fa fa-user"></i>
                                                <input type="radio" name="type" value="etudiant" id="etudiant" checked> Etudiant
                                            </label>

                                            <label><i class="fa fa-user-secret"></i>
                                                <input type="radio" name="type" value="comite" id="admin"> Délégué
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" name="password" class="form-control" placeholder="Mot de passe"/>
                                    </div>
                                    {!! $errors->first('password','<span class="help-block"> :message </span>') !!}
                                </div>
                                <div class="form-group {{$errors->has('passwordConfirm') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                        <input type="password" name="passwordConfirm" class="form-control" placeholder="Confirmez le mot de passe"/>
                                    </div>
                                    {!! $errors->first('passwordConfirm','<span class="help-block"> :message </span>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class='btn btn-success'>Ajouter une image
                                    <input type="file" name="image" class="btnUpload" id="image" size="40"
                                           onchange='$("#upload-file-info").html($(this).val());'>
                                    <input type="hidden" name="photo" id="photo"/>
                                </a>
                                &nbsp;
                                <span class='label label-info' id="upload-file-info"></span>
                            </div>
                            <div class="col-md-6">

                                <div class="col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->


@endsection