@extends('layouts.admin',['titre' => 'Gestion des Postes'])

@section('styles')

<style>
    h3{
        padding: 0 0 0 33%;
        color: #55a;
    }
</style>
    
@endsection

@section('content')

    <div class="row">

        <div class="col-lg-12">

            <!-- Input addon -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="title">Enregistrement des postes de vote</h3>
                </div>
                <div class="box-body">
                    <br>
                    <form action="" method="post" class="col-md-6 col-md-offset-3" enctype="multipart/form-data">
                        @include('flash-message')

                        @csrf
                        <div class="form-group {{$errors->has('idBureau') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <select name="idBureau" id="classe" class="form-control">
                                    <option value=""> Bureau de vote </option>
                                    @foreach(\App\Bureau::all() as $bureau)
                                        <option {{(old('idBureau') == $bureau->idBureau ) ? 'selected' : ''}} value="{{$bureau->idBureau}}"> {{$bureau->code}} -- {{$bureau->nom}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {!! $errors->first('idBureau','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('code') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                <input type="text" name="code" class="form-control" placeholder=" Code du poste" value="{{old('code')}}"/>
                            </div>
                            {!! $errors->first('code','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('login') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
                                <input type="text" name="login" class="form-control" placeholder="Login" value="{{old('login')}}"/>
                            </div>
                            {!! $errors->first('login','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Mot de passe" />
                            </div>
                            {!! $errors->first('password','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('passwordConfirm') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="passwordConfirm" class="form-control" placeholder="Confirmer Mot de passe"/>
                            </div>
                            {!! $errors->first('passwordConfirm','<span class="help-block"> :message </span>') !!}
                        </div>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->


@endsection