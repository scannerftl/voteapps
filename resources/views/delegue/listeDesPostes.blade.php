@extends('layouts.admin',['titre' => 'Gestion des Postes'])

@section('styles')

    <style>
        h3{
            padding: 0 0 0 33%;
            color: #55a;
        }
    </style>
    
@endsection

@section('content')

    <!-- Info boxes -->
    <div class="row">

        <div class="col-md-12">

            <div class="box box-info">
                @include('flash-message')


                <div class="box-header">
                    <h3 class="title"> Liste des postes de vote </h3>
                </div><!-- /.box-header -->

                <div class="box-body">
                    <div class="table-responsive col-md-8 col-md-offset-2">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>N°</th>
                                <th>Code</th>
                                <th>Login</th>
                                <th> Bureau </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($postes as $p)
                                <tr id="item{{$p->idPoste}}">
                                    <td> {{$p->idPoste}} </td>
                                    <td> {{$p->code}} </td>
                                    <td>{{$p->login}}</td>
                                    <td>{{$p->idBureau}} </td>
                                    <td>
                                        <a class="btn btn-warning btn-xs" onclick="modifyUser({{$p->idPoste}})" data-toggle="tooltip" id="modalBtnEdit" title="Modifier"><span class="fa fa-edit"></span></a>


                                        <a class="btn btn-danger btn-xs btnDel" data-toggle="tooltip" id="BtnDel" onclick="deleteUser({{$p->idPoste}})" title="supprimer"><span class="fa fa-times"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            <!-- endforeach -->
                            </tbody>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col-lg-12 -->

    </div><!-- /.row -->

    <!-- Modal -->
    <div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Informations du candidat </h4>
                </div>
                <div class="modal-body editModal">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('scripts')
    <script src={{asset("source/assets/plugins/datatables/jquery.dataTables.min.js")}}></script>
    <script src={{asset("source/assets/plugins/datatables/dataTables.bootstrap.min.js")}} type="text/javascript"></script>
    <script type="text/javascript">

        $(function () {
            $('#example1').dataTable();
        })

        function modifyUser(id){
            var token = '{{csrf_token()}}';
            $.ajax({
                url: '/postes/modifier/'+id,
                data: {
                    _token : token
                },
                method : 'post',
                success:function (page) {

                    $('.editModal').html(page);
                    $("#editModal").modal();
                }
            });
        };


        function deleteUser(id){
            if(confirm("Voulez vous vraiment supprimer ce poste?")){
                var token = '{{csrf_token()}}';
                $.ajax({
                    url: '/postes/update/'+id,
                    data: {
                        _token : token,
                        idDelete: id
                    },
                    method : 'put',
                    success:function (data) {
                        if(data == '1') $('#item'+id).fadeOut();
                        else alert("Ce poste est en fonction vous ne pouvez pas le supprimer")
                    }
                });
            }
        };


    </script>

@endsection
