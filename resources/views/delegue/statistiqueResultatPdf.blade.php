<?php
use Codedge\Fpdf\Fpdf\Fpdf ;

//****************creation de la page--------------------------------------------------
$pdf=new Fpdf() ;

//'P' c'est la page en portrai. en paysage c'est 'L'. 'mm' c'est l'unite de mesure 'A4' c'est le format

$pdf->AddPage();
//****************creation de la page--------------------------------------------------

//---------------------debut header ------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
$pdf->SetFont('Arial','B',7); //  SetFont(). On choisit de l'Arial gras en taille 16 :
$pdf->Cell(12);
$pdf->Cell(0,10,"REPUBLIQUE DU CAMEROUN",'C');
$pdf->Ln(0);
$pdf->Cell(132);
$pdf->Cell(0,10,"REPUBLIC OF CAMEROON",'C');

//-----------------------------------------------------------------------------------------------------------------------
$pdf->Ln(4);
$pdf->SetFont('Arial','B',5);
$pdf->Cell(20);
$pdf->Cell(0,7,"PAIX - TRAVAIL - PATRIE",'C');
$pdf->Ln(0);
$pdf->SetFont('Arial','B',5);
$pdf->Cell(134);
$pdf->Cell(0,7,"PEACE - WORK - FATHERLAND",'C');
//-----------------------------------------------------------------------------------------------------------------------------
$pdf->Ln(2);
$pdf->Cell(16);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"---------------------------------------------",'C');
$pdf->Ln(0);
$pdf->Cell(129);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"-----------------------------------------------------",'C');
//----------------------------------------------------------------------------------------------------------------------------
$pdf->Ln(2);

$pdf->SetFont('Arial','B',7); //  SetFont(). On choisit de l'Arial gras en taille 16 :
$pdf->Cell(10);
$pdf->Cell(0,10,	"MINISTERE DES ENSEIGNEMENT SUPERIEUR ",'C');
$pdf->Ln(0);
$pdf->Cell(128);
$pdf->SetFont('Arial','B',7); //  SetFont(). On choisit de l'Arial gras en taille 16 :
$pdf->Cell(0,10,"MINISTRY OF HIGHER EDUCATION",'C');
$pdf->Ln(3);
$pdf->Cell(11);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"----------------------------------------------------------------",'C');
$pdf->Ln(0);
$pdf->Cell(129);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"-------------------------------------------------------",'C');
////---------------------------------------------------------------------------------------------------------------------------
$pdf->Ln(1);
$pdf->Cell(20);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,10,utf8_decode("Année scolaire: "."2019 / 2020"),'C');
$pdf->Ln(0);
$pdf->Cell(135);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,10,utf8_decode("School year: "."2019 / 2020"),'C');

$pdf->Ln(8);

//-----------------------------------------------------------------------------------------------------------------------
$pdf->Ln(7);
$pdf->SetFont('Arial','I',8);
//------------------------------------------------------------------
$pdf->Cell(0,7,utf8_decode("Situé à: "."YAOUNDE"." :"."NKOL LA'ANGA".".      "),'C');
//--------------------------------------------------------------------------------------------------------------------------
$pdf->Ln(0);
$pdf->Cell(70);
$pdf->SetFont('Arial','B',8);
$pdf->Cell(0,7,utf8_decode("Observer Proposer Numeriser"),'C');
//------------------------------------------------------------------------------------------
$pdf->Ln(0);
$pdf->Cell(145);
$pdf->SetFont('Arial','I',8);
$pdf->Cell(0,7,"Tel: "."+237 43 26 54 65",'C');

//-------------------------------------------------------------------------------------------
$pdf->Ln(1);
$pdf->Cell(70);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"-------------------------------------------",'C');
//-------------------------------------------------------------------------------------------
$pdf->Ln(1);
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,7,"............................................................................................................................................................................................................................................................................................................................",'C');
//****************entete de la page--------------------------------------------------

  $pdf->Image('/var/www/html/eleciai.cm/resources/views/logo_iai.jpeg', $x=90, $y=5,30,30,'jpeg');

//----------------------------fin header


//------------------------------Contenu de la page------------------------------



$pdf->Ln(10);
$pdf->Cell(60);
$pdf->SetFont('Times','B',14);
$pdf->Cell(0,7, utf8_decode(" RESULTAT FINALE DES VOTES  "),'C');
///-----------------------------------------------------------------------------
$pdf->Ln(5);
$pdf->Cell(70);
$pdf->SetFont('Times','B',10);
//-----------------------------------------------------------------------------
$pdf->Ln(0);
$pdf->Cell(145);
$pdf->SetFont('Times','I',10);
$pdf->Cell(0,7,"",'C');
//------------------------------------------------------------------------
//------------------------------------------------------------------------
$pdf->Ln(10);
$pdf->SetFont('Arial','',14);

$data=array();



for($i=0;$i<=sizeof($resultat[0]['parBureau'])-1;$i++){
  
  $pdf->SetFont('Arial', 'BIU', 12);
  $pdf->Cell(0, 5, $resultat[0]['parBureau'][$i]['nomBureau'], 0, 2, 'L');
  $pdf->Ln(8);
  
 // foreach($resultat[0]['parBureau'][$i]['candidats'] as  $value) {

    $pdf->Ln(5);
    $pdf->SetFont('Arial','',8);
    $col= array('partie'=>20,'bureau'=>30,'votes'=>20,'pourcentage'=>40);
    $pdf->Table($resultat[0]['parBureau'][$i]['candidats'],$col);
    $pdf->ln(5); 
/*
  $data[ $value->partie] =  $value->votes;
    $pdf->Cell(35, 3, $value->partie);
    $pdf->Cell(15, 3, $value->votes, 0, 0, 'R');
    $pdf->Cell(18, 3, " voie", 0, 0, 'R');
    $pdf->ln(5); */
   // }
 
/*
$pdf->SetFont('Arial', '', 10);
$valX = $pdf->GetX();
$valY = $pdf->GetY();
$pdf->Ln();
$pdf->SetXY(10, $valY);
$col1=array(100,100,255);
$col2=array(255,100,100);
$col3=array(255,255,100);
$col4=array(100,100,0);
$col5=array(10,100,255);
$col6=array(100,0,0);
$col7=array(0,0,255);
$pdf->DiagCirculaire(60, 40, $data, '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7));
$pdf->SetXY($valX, $valY + 40);
*/

}

  $pdf->SetFont('Arial', 'BIU', 12);
  $pdf->Cell(0, 5, $resultat[0]['parBureau'][0]['lebele_finale'], 0, 2, 'L');
  $pdf->Ln(8);
 
  foreach($resultat[0]['parBureau'][0]['finale'] as  $value) {

    $data[ $value->partie] =  $value->votes;

    $pdf->Cell(35, 3, $value->partie);
    $pdf->Cell(15, 3, $value->votes, 0, 0, 'R');
    $pdf->ln(5);
    }
    
  $pdf->SetFont('Arial', '', 10);
$valX = $pdf->GetX();
$valY = $pdf->GetY();
$pdf->Ln();
$pdf->SetXY(90, $valY);
$col1=array(100,100,255);
$col2=array(255,100,100);
$col3=array(255,255,100);
$col4=array(100,100,0);
$col5=array(10,100,255);
$col6=array(100,0,0);
$col7=array(0,0,255);
$pdf->DiagCirculaire(100, 80, $data, '%l (%p)', array($col1,$col2,$col3,$col4,$col5,$col6,$col7));
$pdf->SetXY($valX, $valY + 40);
 



//fin remplissage du bilan detaille--------------------------------------------


$pdf->Output();

exit();
