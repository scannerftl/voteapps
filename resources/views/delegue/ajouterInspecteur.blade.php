@extends('layouts.admin',['titre' => 'Gestion des Inspecteurts'])

@section('styles')

    <style>
        h3{
            padding: 0 0 0 33%;
            color: #28f;
        }
    </style>
    
@endsection
@section('content')

    <div class="row">

        <div class="col-lg-12">

            <!-- Input addon -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="title">Enregistrement des Inspecteurs</h3>
                </div>
                <div class="box-body">
                    <br>
                    <form action="" method="post" class="col-md-6 col-md-offset-3" enctype="multipart/form-data">
                        @include('flash-message')

                        @csrf
                        <div class="form-group {{$errors->has('idEtudiant') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <select name="idEtudiant" id="classe" class="form-control">
                                    <option value=""> Inspecteur </option>
                                    @foreach(\App\Etudiant::listeDesPotentielsInspecteurs() as $etudiant)
                                        <option {{(old('idEtudiant') == $etudiant->idEtudiant ) ? 'selected' : ''}} value="{{$etudiant->idEtudiant}}"> {{$etudiant->nom}} {{$etudiant->prenom}} | {{$etudiant->classe}}</option>
                                    @endforeach
                                </select>
                            </div>
                            {!! $errors->first('idEtudiant','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('idCandidat') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <select name="idCandidat" id="classe" class="form-control">
                                    <option value=""> Parti de l'inspecteur </option>
                                    @foreach(\App\Candidat::all() as $candidat)
                                        <option {{(old('idCandidat') == $candidat->idCandidat ) ? 'selected' : ''}} value="{{$candidat->idCandidat}}"> {{$candidat->partie}} </option>
                                    @endforeach
                                </select>
                            </div>
                            {!! $errors->first('idEtudiant','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="form-group {{$errors->has('idBureau') ? 'has-error' : ''}}">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user-secret"></i></span>
                                <select name="idBureau" id="classe" class="form-control">
                                    <option value=""> Bureau de l'inspecteur </option>
                                    @foreach(\App\Bureau::all() as $bureau)
                                        <option {{(old('idBureau') == $bureau->idBureau ) ? 'selected' : ''}} value="{{$bureau->idBureau}}"> {{$bureau->code}} -- {{$bureau->nom}} </option>
                                    @endforeach
                                </select>
                            </div>
                            {!! $errors->first('idBureau','<span class="help-block"> :message </span>') !!}
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->


@endsection