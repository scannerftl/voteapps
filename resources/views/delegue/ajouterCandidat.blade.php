@extends('layouts.admin',['titre' => 'Gestion des Candidats'])

@section('styles')

    <style xmlns="http://www.w3.org/1999/html">
        .btnUpload {
            position:absolute;
            z-index:2;
            top:0;
            left:0; 
            filter: alpha(opacity=0);
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            opacity:0;
            background-color:transparent;
            color:transparent;
        }

        h3{
            padding: 0 0 0 33%;
            color: #169;
        }
    </style>
@endsection
@section('content')

    <div class="row">

        <div class="col-lg-12">

            <!-- Input addon -->
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="title">Enregistrement des Candidats</h3>
                </div>
                <div class="box-body">
                    <br>
                    <form action="" method="post" class="col-md-8 col-md-offset-2" enctype="multipart/form-data">
                        @include('flash-message')

                        @csrf
                                <div class="form-group {{$errors->has('idEtudiant') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <select name="idEtudiant" id="classe" class="form-control">
                                            <option value=""> Président du parti </option>
                                            @foreach(\App\Etudiant::listeDesPotentielsCandidats() as $etudiant)
                                                <option {{(old('idEtudiant') == $etudiant->idEtudiant ) ? 'selected' : ''}} value="{{$etudiant->idEtudiant}}"> {{$etudiant->nom}} {{$etudiant->prenom}} | {{$etudiant->classe}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {!! $errors->first('idEtudiant','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('partie') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-group"></i></span>
                                        <input type="text" name="partie" class="form-control" placeholder="Nom du Parti" value="{{old('partie')}}"/>
                                    </div>
                                    {!! $errors->first('partie','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('slogan') ? 'has-error' : ''}}">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-hand-peace-o"></i></span>
                                        <input type="text" name="slogan" class="form-control" placeholder="Slogan" value="{{old('slogan')}}"/>
                                    </div>
                                    {!! $errors->first('slogan','<span class="help-block"> :message </span>') !!}
                                </div>

                                <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
                                    <label for="description" class="contro-label"> Description </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                                        <textarea  name="description" rows="5"  class="form-control"> {{old('description')}} </textarea>
                                    </div>
                                    {!! $errors->first('description','<span class="help-block"> :message </span>') !!}
                                </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class='btn btn-success'>Ajouter une image
                                    <input type="file" name="image" class="btnUpload" id="image" size="40"
                                           onchange='$("#upload-file-info").html($(this).val());'>
                                    <input type="hidden" name="photo" id="photo"/>
                                </a>
                                &nbsp;
                                <span class='label label-info' id="upload-file-info"></span>
                            </div>
                            <div class="col-md-6">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col -->


    </div><!-- /.row -->


@endsection
