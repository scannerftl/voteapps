<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vote extends Model
{
    protected $table = 'votes';
    protected  $guarded = ['idVote'];
    public $timestamps = null;
    protected $primaryKey = 'idVote';
    protected $dates = ['dateCreation'];

    public static function getVotes($id){

        return static::where('idCandidat',$id)->count();
    }

    public static function getInspectorResult($id){

        return DB::select(" SELECT COUNT(votes.idVote) AS nbrVote,bureaux.code, bureaux.idBureau, bureaux.nom, candidats.logo, candidats.partie, candidats.slogan, candidats.description, candidats.idEtudiant, inspecteurs.idCandidat 
                                FROM votes 
                                JOIN inspecteurs ON inspecteurs.idCandidat = votes.idCandidat 
                                JOIN candidats ON candidats.idCandidat = inspecteurs.idCandidat 
                                JOIN bureaux ON bureaux.idBureau = inspecteurs.idBureau  
                                WHERE inspecteurs.idInspecteur = $id
                                AND votes.idPoste = 
                                    (SELECT idPoste FROM postes 
                                        WHERE postes.idBureau = inspecteurs.idBureau 
                                        AND postes.idPoste = votes.idPoste)")[0];
    }

    // Les résultats des votes par Bureau de vote
    public static function getResultsbyBureau($id){
        return DB::select("SELECT 
                                      COUNT(votes.idCandidat) *100 / (
                                          SELECT COUNT(votes.idCandidat) 
                                            FROM votes 
                                            JOIN postes ON votes.idPoste = postes.idPoste 
                                            JOIN bureaux ON postes.idBureau = bureaux.idBureau  
                                            WHERE bureaux.idBureau = $id) AS pourcentage, 
                                    COUNT(votes.idCandidat) AS votes, slogan, partie, bureaux.code AS bureau, bureaux.nom 
                                    FROM votes 
                                    JOIN candidats ON votes.idCandidat = candidats.idCandidat 
                                    JOIN postes ON votes.idPoste = postes.idPoste 
                                    JOIN bureaux ON bureaux.idBureau = postes.idBureau 
                                    WHERE bureaux.idBureau = $id 
                                    GROUP BY votes.idCandidat");
    }

    // Les résultats des votes complet
    public static function getResult(){
        return DB::select("SELECT 
                                      COUNT(votes.idCandidat) * 100 / (
                                        SELECT COUNT(idVote) 
                                        FROM votes) AS pourcentage, 
                                  COUNT(votes.idCandidat) as votes,slogan, partie 
                                  FROM votes 
                                  JOIN candidats ON votes.idCandidat = candidats.idCandidat 
                                  GROUP BY votes.idCandidat");
    }

    // Les résultats des votes des étudiants
    public static function getEtudiantsResult(){
        return DB::select("SELECT etudiants.codeVote, candidats.partie,postes.code AS poste, bureaux.code AS bureau, votes.dateCreation AS date 
                                      FROM votes 
                                      JOIN etudiants ON votes.idEtudiant = etudiants.idEtudiant 
                                      JOIN candidats ON candidats.idCandidat = votes.idCandidat 
                                      JOIN postes ON postes.idPoste = votes.idPoste 
                                      JOIN bureaux ON bureaux.idBureau = postes.idBureau");
    }

    // Les étudiants qui n'ont pas voté
    public static function getAbsentStudent(){
        return DB::select("SELECT etudiants.nom, etudiants.prenom, etudiants.photo, etudiants.tel, etudiants.email, classes.code AS classe 
                                      FROM etudiants JOIN classes ON classes.idClasse = etudiants.idClasse  
                                      WHERE NOT EXISTS (SELECT * 
                                            FROM votes 
                                            WHERE votes.idEtudiant = etudiants.idEtudiant) 
                                      AND TYPE = 'etudiant'");
    }


}
