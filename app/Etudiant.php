<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Etudiant extends Model
{
    protected $table = 'etudiants';
    protected  $guarded = ['idEtudiant'];
    public $timestamps = null;
    protected $primaryKey = 'idEtudiant';
    protected $dates = ['dateCreation'];


    public static function listeDesEtudiants(){
        return static::whereType('etudiant')->get();
    }
    public static function listeDelegues(){
        return static::whereType('comite')->get();
    }

    public static function listeDesVoteurs(){
        return DB::select("SELECT etudiants.*, classes.code as classe
                                    FROM etudiants
                                    JOIN classes ON classes.idClasse = etudiants.idClasse
                                    WHERE type = 'etudiant'
                                    AND visibility = 'visible'");
    }

    public static function listeDesEtudiantsEligible(){
        return DB::select("SELECT DISTINCT etudiants.* 
                                    FROM etudiants WHERE NOT EXISTS 
                                        (SELECT * FROM votes 
                                          WHERE etudiants.idEtudiant = votes.idEtudiant) 
                                    AND type='etudiant'
                                    AND visibility = 'visible'");
    }

    public static function listeDesPotentielsCandidats(){
        return DB::select("SELECT DISTINCT E.idEtudiant, E.matricule, E.email, E.nom, E.prenom, classes.code as classe 
                                    FROM etudiants E
                                    JOIN classes ON E.idClasse = classes.idClasse 
                                    WHERE NOT EXISTS 
                                        (SELECT * FROM candidats C 
                                        WHERE E.idEtudiant = C.idEtudiant)
                                    AND type = 'etudiant'
                                    AND visibility = 'visible'");
    }

    public static function listeDesPotentielsInspecteurs(){
        return DB::select("SELECT DISTINCT E.idEtudiant, E.nom, E.prenom, classes.code as classe 
                                    FROM etudiants E
                                    JOIN classes ON E.idClasse = classes.idClasse 
                                    WHERE NOT EXISTS 
                                        (SELECT * FROM inspecteurs I 
                                        WHERE E.idEtudiant = I.idEtudiant)
                                    AND type = 'etudiant'
                                    AND visibility = 'visible'");
    }

    public static function getAtuhUser(){
        return static::where('idEtudiant',session('userId'))->first();
    }

    public static function getEtudiantById($id){
        return static::where('idEtudiant',$id)->first();
    }

}
