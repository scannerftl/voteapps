<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poste extends Model
{
    protected $table = 'postes';
    protected  $guarded = ['idPoste'];
    public $timestamps = null;
    protected $primaryKey = 'idPoste';

    public static function listeDesPostes(){
        return static::all();
    }

    public static function getPosteById($id){
        return static::find($id);
    }
}
