<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Classe extends Model
{
    protected $table = 'classes';
    protected $guarded = ['idClasse'];

    public static function listeDesClasses(){
        return static::all();
    }

    public static function getClasseById($id){
        return static::where('idClasse', $id)->first();
    }
}
