<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 07/11/19
 * Time: 17:31
 */

namespace App\Http\Controllers;


use App\Inspecteur;

class EtudiantController extends Controller
{
    public function verifEtudiant()
    {
        return ($this->userExist() && session('type') == 'etudiant');
    }

    public function verifInspecteur(){
        return($this->userExist() && Inspecteur::where('idEtudiant',session('userId'))->first());
    }
}