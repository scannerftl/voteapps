<?php

namespace App\Http\Controllers;

use App\Bureau;
use App\Etudiant;
use App\Inspecteur;
use App\Notification;
use App\Poste;
use App\Vote;
use Illuminate\Http\Request;

class connexionController extends Controller
{

    public function login(){
        if($this->userExist()){
            if(session('type') == 'comite') return redirect()->route('delegue.dashboard');
            else return redirect()->route('pageInspecteur');
        }
        if($this->posteExist()) return redirect()->route('login.vote');

        return view('login');
    }

    public function loginPost(Request $request){
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $etudiant = Etudiant::whereEmail($request->email)->wherePassword(sha1($request->password))->first();

        if($etudiant){
            if($etudiant->type == 'comite'){
                $this->logUser($etudiant);
                return redirect()->route('delegue.dashboard');
            }

            if(Inspecteur::where('idEtudiant',$etudiant->idEtudiant)){
                $this->logUser($etudiant);
                return redirect()->route('pageInspecteur');
            }
        }

        return redirect()->back()->with('error', 'Combinaison email / Mot de passe incorrect');
    }

    public function loginEtudiant(){
        if(!$this->posteExist()) return redirect()->route('login.poste');

        return view('loginEtudiant');
    }

    public function loginEtudiantPost(Request $request){
        $this->validate($request,[
           'email' => 'required|email',
           'codeVote' => 'required',
           'password' => 'required'
        ]);

        $etudiant = Etudiant::whereEmail($request->email)->whereVisibility('visible')->wherePassword(sha1($request->password))->first();
        if($etudiant){
            if($etudiant->type == 'comite') return redirect()->back()->with('warning',"Les délégués n'ont pas le droit de voter!");
            else{
                if($etudiant->codeVote){
                    if($etudiant->codeVote == $request->codeVote){
                        $verifVote = Vote::where('idEtudiant',$etudiant->idEtudiant)->first();
                        if(!$verifVote){
                            $this->logUser($etudiant);
                        } else {
                            // Notification
                            Notification::addNotification(
                                'etudiants',
                                $etudiant->idEtudiant,
                                'forcer vote',
                                3,
                                $etudiant->nom.' a essayé de voter deux fois sur le poste '.session('code').' du bureau '. Bureau::find(session('idBureau'))->code
                            );

                            return redirect()->back()->with('error', 'Vous avez déja voté veuillez libérer le poste svp');
                        }
                        $this->annullerEssaiesDeConnexion();
                        return redirect()->route('pageDeVote'); // connexion avec succès
                    }else{

                        $this->verifierEssaiesDeConnexion();
                        return redirect()->back()->with('warning','Code de vote non valide'); // le code n'est pas valide
                    }
                }else{
                    //Notification
                    Notification::addNotification(
                        'etudiants',
                        $etudiant->idEtudiant,
                        'voter sans code de vote',
                        '2',
                        $etudiant->nom.' essaie de se connecter sur le poste '.session('code').' du bureau '. Bureau::find(session('idBureau'))->code.' sans avoir été activé'
                    );

                    return redirect()->back()->with('error',"Vous n'avez pas de code de vote! Veuillez vous rapprocher d'un délégué pour en avoir un"); // pas de code de vote donc pas encore créé
                }
            }
        }

        $this->verifierEssaiesDeConnexion();
        return redirect()->back()->with('error',"Combinaison email/mot de passe incorrecte");  // Nom d'utilisateur et /ou mot de passe incorect
    }

    public function loginPoste(){
        if($this->posteExist()) return redirect()->route('login.vote');


        return view('loginPoste');
    }

    public function loginPostePost(Request $request){
        $this->validate($request,[
            'login' => 'required',
            'password' => 'required'
        ]);

        $poste = Poste::whereLogin($request->login)->wherePassword(sha1($request->password))->first();
        if($poste){
            if($poste->etat == 'actif'){
                return redirect()->back()->with('error', " Ce poste est déja en cours d'utilisation");
            }else{
                $poste->etat = 'actif';
                $poste->save();
                $this->logPoste($poste);
                return redirect()->route('login.vote');
            }
        }

        return redirect()->back()->with('error',"Informations du poste incorrect");
    }

    public function logoutPoste(){
        $poste = Poste::findOrFail(session('idPoste'));
        $poste->etat = 'inactif';
        $poste->save();

        $this->unlogPoste();

        return redirect()->route('login.poste');
    }

    public function logout(){
        $this->unlogUser();
        return redirect(route('login'))->with('info','Vous êtes déconnecté');
    }

    public function logoutUser(){
        $this->unlogUser();
        return redirect()->route('login.vote')->with('success'," Votre vote a été enregistré avec succès!");
    }
}
