<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 07/11/19
 * Time: 15:22
 */

namespace App\Http\Controllers;

use PHPMailer\PHPMailer\PHPMailer;

class DelegueController extends Controller
{

    public function verifDelegue()
    {
        return ($this->userExist() && session('type') == 'comite');
    }

    public function sendMail($email, $sujet, $vue, $nom, $data = null){

        $message = view($vue,compact('data'));

        $mail = new PHPMailer(true);
            $mail->isSMTP();
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth =true;
            $mail->SMTPSecure = env('MAIL_ENCRYPTION');
            $mail->Host = env('MAIL_HOST'); //gmail has host > smtp.gmail.com
            $mail->Port = env('MAIL_PORT'); //gmail has port > 587 . without double quotes
            $mail->Username = env('MAIL_USERNAME'); //your username. actually your email
            $mail->Password = env('MAIL_PASSWORD'); // your password. your mail password
            $mail->setFrom(env('APP_EMAIL'), env('APP_NAME'));
            $mail->Subject = $sujet;
            $mail->MsgHTML($message);
            $mail->addAddress($email , $nom);
            if(!$mail->send()){
                dd($mail->ErrorInfo);
            }
    }

    public function getIBBLik($file){
        $handle = fopen($file, "r");
        $image = fread($handle, filesize($file));

        $ibb = curl_init();
        $data   = array('image' => base64_encode($image));
        curl_setopt($ibb, CURLOPT_URL, 'https://api.imgbb.com/1/upload?key=d8d35ec6a51894c9f2c84a02b61cf806');
        curl_setopt($ibb, CURLOPT_POST, 1);
        curl_setopt($ibb, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ibb, CURLOPT_SAFE_UPLOAD, true);
        curl_setopt($ibb, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ibb);
        curl_close($ibb);
        $result = json_decode($output,true);

        return $result['data']['url'];
    }
}
