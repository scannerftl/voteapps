<?php

namespace App\Http\Controllers\Delegue;

use App\Action;
use App\Candidat;
use App\Etudiant;
use App\Http\Controllers\DelegueController;
use Illuminate\Http\Request;

class candidatsController extends DelegueController
{
    public function candidatList(){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $candidats = Candidat::listeDesCandidats();

        return view('delegue.listeDesCandidats',compact('candidats'));
    }

    public function ajouterCandidat(){

        if(!$this->verifDelegue()) return redirect()->route('login');

        return view('delegue.ajouterCandidat');
    }

    public function afficherCandidat($id){
        $cdd = Candidat::getCandidatById($id);
        return view('ajax.afficherCandidat', compact('cdd'));
    }

    public function verifierCandidat(Request $request){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $this->validate($request,[
            'slogan' => 'required|unique:candidats,slogan',
            'partie' => 'required|unique:candidats,partie',
            'idEtudiant' => 'required|unique:candidats,idEtudiant',
            'description' => 'required|min:20'
        ]);

        $candidat = Candidat::create($request->only('idEtudiant','slogan','description','partie'));
        if($candidat){
            //upload de la photo
            $photo = $request->file('image');
            if($photo){
                $path = 'source/user/candidats/'.$candidat->partie.'/';
                $fileName = uniqid('student-').'.'.$photo->extension();
                $name = $path.$fileName;
                $photo->move($path,$name);
                $candidat->logo = $name;
                $candidat->logoIbbLink = $this->getIBBLik($name);
            }
            $candidat->save();

            //Enregistrement des logs
            Action::addAction(session('userId'), 'candidats', $candidat->idCandidat, 'creer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a cree le parti: '.$candidat->partie);
        }

        return redirect()->back()->with('info',"Le parti $candidat->partie a été enregistré avec succès");
    }

    public function modifierCandidat($id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $cdd = Candidat::getCandidatById($id);
        return view('ajax.modifierCandidat', compact('cdd'));
    }

    public function updateCandidat(Request $request,$id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        //suppression de l'utilisateur
        if($request->idDelete){

            Action::addAction(session('userId'), 'candidats', $id, 'supprimer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a supprime le parti: '.Candidat::find($id)->partie);

            $result = Candidat::destroy($id);

            return ($result) ? '1' : '0';
        }

        $this->validate($request,[
            'slogan' => 'required',
            'partie' => 'required',
            'description' => 'required|min:20'
        ]);

        $candidat = Candidat::findOrFail($id);
        if($candidat){
            //upload de la photo
            $photo = $request->file('image');
            if($photo){
                $path = 'source/user/candidats/'.$candidat->partie.'/';
                $fileName = uniqid('student-').'.'.$photo->extension();
                $name = $path.$fileName;
                $photo->move($path,$name);
                $candidat->logo = $name;
                $candidat->logoIbbLink = $this->getIBBLik($name);
            }
            $candidat->update($request->only('slogan','description','partie'));

            //Enregistrement des logs
            Action::addAction(session('userId'), 'candidats', $candidat->idCandidat, 'modifier',
            Etudiant::getEtudiantById(session('userId'))->nom.' a modifie le parti: '.$candidat->partie);
        }

        return redirect()->back()->with('info',"Les informations du parti on bien été mis à jour!");
    }

}
