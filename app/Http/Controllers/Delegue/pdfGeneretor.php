<?php

namespace App\Http\Controllers\Delegue;

use App\Etudiant;
use App\Vote;
use App\Action;
use App\Candidat;
use App\Bureau ;
use App\Inspecteur;
use App\Poste;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class pdfGeneretor extends Controller
{
    public  function  listeEtudiantsPdf(){
        $etudiants= Etudiant::listeDesVoteurs();
        return view('delegue.listeEtudiantsPdf',compact('etudiants'));
    }


    public  function  listeCandidatsPdf(){
        $candidats= Candidat::listeDesCandidats();
        
        return view('delegue.listeCandidatsPdf',compact('candidats'));
    }

    public  function  statistiqueVotePdf(){
        $votes= Vote::getEtudiantsResult();           
        return view('delegue.statistiqueVotePdf',compact('votes'));
    }

    public  function  fichierLogPdf(){
        $logs= Action::all();
        return view('delegue.fichierLogPdf',compact('logs'));
    }

    public  function  statistiqueResultatPdf()
    {

         $bureau = Bureau::listeDesBureaux();         
        $resultat=array();
       $resultat[0]['parBureau']=array();
       $resultat[0]['parBureau'][0]['candidats']=array();
       $resultat[0]['parBureau'][0]['candidats_all']=array();
       $resultat[0]['parBureau'][0]['finale']=array();
       $resultat[0]['parBureau'][0]['lebele_finale']="Statistiques globales";
       $resultat[0]['parBureau'][0]['finale']=Vote::getResult();
       $resultat[0]['parBureau'][0]['candidats_all']=Candidat::listeDesCandidats();
       
       for($i=0;$i<=sizeof($bureau)-1;$i++){
        $resultat[0]['parBureau'][$i]['idBureau']=$bureau[$i]['idBureau'];
        $resultat[0]['parBureau'][$i]['nomBureau']=$bureau[$i]['nom'];  
        $resultat[0]['parBureau'][$i]['candidats']=Vote::getResultsbyBureau($bureau[$i]['idBureau']);  
        
                            
    } 
 return view('delegue.statistiqueResultatPdf',compact('resultat'));

}
public  function  listeInspecteursPdf(){
    $inspecteurs= Inspecteur::listeDesInspecteurs();
    return view('delegue.listeInspecteursPdf',compact('inspecteurs'));
}

public  function  listePostesPdf(){
    $postes= Poste::listeDesPostes();
    return view('delegue.listePostesPdf',compact('postes'));
}

public  function  listeBureauxPdf(){
    $bureaux= Bureau::listeDesBureaux();
    return view('delegue.listeBureauxPdf',compact('bureaux'));
}

public  function  listeDeleguesPdf(){
    $delegues= Etudiant::listeDelegues();
    return view('delegue.listeDeleguesPdf',compact('delegues'));
}
}