<?php

namespace App\Http\Controllers\Delegue;

use App\Action;
use App\Candidat;
use App\Etudiant;
use App\Http\Controllers\DelegueController;
use App\Inspecteur;
use App\Vote;
use Hamcrest\Core\IsIdentical;
use Illuminate\Http\Request;

class inspecteursController extends DelegueController
{

    public function inspecteurList(){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $inspecteurs = Inspecteur::listeDesInspecteurs();

        return view('delegue.listeDesInspecteurs',compact('inspecteurs'));
    }

    public function ajouterInspecteur(){
        if(!$this->verifDelegue()) return redirect()->route('login');


        return view('delegue.ajouterInspecteur');
    }

    public function verifierInspecteur(Request $request){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $this->validate($request,[
            'idCandidat' => 'required',
            'idEtudiant' => 'required|unique:inspecteurs,idEtudiant',
            'idBureau' => 'required'
        ]);

        $inspecteur = Inspecteur::create($request->only('idEtudiant','idBureau','idCandidat'));


        Action::addAction(session('userId'), 'inspecteurs', $inspecteur->idInspecteur, 'creer',
        Etudiant::getEtudiantById(session('userId'))->nom.' a cree l\'inspecteur: '.Etudiant::find($inspecteur->idEtudiant)->nom);

        return redirect()->back()->with('info',"l'inspecteur a été ajouté avec succès");
    }

    public function modifierInspecteur($id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $ispt = Inspecteur::getInspecteurById($id);
        return view('ajax.modifierInspecteur', compact('ispt'));
    }

    public function afficherInspecteur($id){
        $ispt = Inspecteur::getInspecteurById($id);
        return view('ajax.afficherInspecteur', compact('ispt'));
    }

    public function updateInspecteur(Request $request,$id){

        //suppression de l'utilisateur
        if($request->idDelete){
            
            Action::addAction(session('userId'), 'inspecteurs', $id, 'supprimer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a supprime l\'inspecteur: '.Inspecteur::getInspecteurById($id)->nom);

            $result = Inspecteur::destroy($id);

            return ($result) ? '1' : '0';
        }

        $this->validate($request,[
            'idCandidat' => 'required',
            'idEtudiant' => 'required',
            'idBureau' => 'required'
        ]);

        $inspecteur = Inspecteur::findOrFail($id);
        if($inspecteur){
            $inspecteur->update($request->only('slogan','description','partie'));


            Action::addAction(session('userId'), 'inspecteurs', $inspecteur->idInspecteur, 'modifier',
            Etudiant::getEtudiantById(session('userId'))->nom.' a modifie l\'inspecteur: '.Etudiant::find($inspecteur->idEtudiant)->nom);


        }

        return redirect()->back()->with('info',"Les informations de l'inspecteur on bien été modifiées");
    }
}
