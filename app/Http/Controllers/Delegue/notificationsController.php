<?php

namespace App\Http\Controllers\Delegue;

use App\Http\Controllers\DelegueController;
use App\Notification;
use Illuminate\Http\Request;

class notificationsController extends DelegueController
{
    public function markAsRead($id){
        $notification = Notification::find($id);
        $notification->etat = 'read';
        $notification->save();
        return $notification->whereEtat('unread')->count();
    }

    public function updateNotificationsList(){
        return view('ajax.notifications');
    }

    public function notifications(){
        $notifications = Notification::all();

        return view('notifications', compact('notifications'));
    }
}
