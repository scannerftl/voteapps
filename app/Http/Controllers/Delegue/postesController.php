<?php

namespace App\Http\Controllers\Delegue;

use App\Action;
use App\Etudiant;
use App\Http\Controllers\DelegueController;
use App\Poste;
use Illuminate\Http\Request;

class postesController extends DelegueController
{
    public function posteList(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        $postes = Poste::listeDesPostes();

        return view('delegue.listeDesPostes',compact('postes'));
    }

    public function ajouterPoste(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        return view('delegue.ajouterPoste');
    }

    public function verifierPoste(Request $request){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $this->validate($request,[
            'idBureau' => 'required',
            'code' => 'required|unique:postes,code',
            'login' => 'required|unique:postes,login',
            'password' => 'min:6',
            'passwordConfirm' => 'required_with:password|same:password|min:6',
        ]);

        $poste = Poste::create($request->only('code', 'password', 'idBureau', 'login'));
        if($poste){
            $poste->password = sha1($request->password);
            $poste->save();


            Action::addAction(session('userId'), 'postes', $poste->idPoste, 'creer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a cree le poste '.$poste->code);

        }

        return redirect()->back()->with('info',"Le poste $poste->code a été ajouté avec succès");
    }

    public function modifierPoste($id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $pst = Poste::getPosteById($id);
        return view('ajax.modifierPoste', compact('pst'));
    }

    public function updatePoste(Request $request,$id){
        if(!$this->verifDelegue()) return redirect()->route('login');

        
        //suppression du poste
        if($request->idDelete){

            $poste = Poste::find($id);
            if($poste->etat == 'actif'){
                Action::addAction(session('userId'), 'postes', $id, 'supprimer',
                    Etudiant::getEtudiantById(session('userId'))->nom.' a éssayé de  supprimer le poste: '.Poste::find($id)->code.' qui est en fonction');

                return '0';
            } else{

                Action::addAction(session('userId'), 'postes', $id, 'supprimer',
                    Etudiant::getEtudiantById(session('userId'))->nom.' a supprime le poste: '.Poste::find($id)->code);

                $result = Poste::destroy($id);

                return ($result) ? '1' : '0';
            }

        }

        $this->validate($request,[
            'idBureau' => 'required',
            'code' => 'required'
        ]);

        $poste = Poste::findOrFail($id);
        if($poste){

            $poste->update($request->only('idBureau','code'));


            Action::addAction(session('userId'), 'postes', $poste->idPoste, 'modifier',
            Etudiant::getEtudiantById(session('userId'))->nom.' a modifier le poste:  '.$poste->code);


        }

        return redirect()->back()->with('info', "Les informations du poste ont bien été modifiées");
    }
}
