<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 07/11/19
 * Time: 19:52
 */

namespace App\Http\Controllers\Delegue;


use App\Http\Controllers\DelegueController;

class votesController extends DelegueController
{
    public function dashboard(){
        
        if(!$this->verifDelegue()) return redirect()->route('login');

        return view('delegue.dashboard');
    }
}