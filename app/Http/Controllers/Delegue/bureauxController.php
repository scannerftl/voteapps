<?php

namespace App\Http\Controllers\Delegue;

use App\Action;
use App\Bureau;
use App\Etudiant;
use App\Http\Controllers\DelegueController;
use Illuminate\Http\Request;

class bureauxController extends DelegueController
{
    public function bureauList(){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $bureaux = Bureau::listeDesBureaux();

        return view('delegue.listeDesBureaux',compact('bureaux'));
    }

    public function ajouterBureau(){
        if(!$this->verifDelegue()) return redirect()->route('login');


        return view('delegue.ajouterBureau');
    }

    public function verifierBureau(Request $request){
        $this->validate($request,[
            'code' => 'required|unique:bureaux,code',
            'nom' => 'required|unique:bureaux,nom',
        ]);

        $bureau = Bureau::create($request->only('code','nom'));

            
        Action::addAction(session('userId'), 'bureaux', $bureau->idBureau, 'creer',
        Etudiant::getEtudiantById(session('userId'))->nom.' a cree le bureau: '.$bureau->code);


        return redirect()->back()->with('info',"le bureau $bureau->code a été créé avec succès");
    }

    public function modifierBureau($id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        $br = Bureau::getBureauById($id);
        return view('ajax.modifierBureau',compact('br'));
    }

    public function afficherBureau($id){
        $br = Bureau::getBureauById($id);
        return view('ajax.afficherBureau', compact('br'));
    }

    public function updateBureau(Request $request, $id){
        if(!$this->verifDelegue()) return redirect()->route('login');


        //suppression de l'utilisateur
        if($request->idDelete){

            Action::addAction(session('userId'), 'bureaux', $id, 'supprimer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a supprime le bureau: '.Bureau::find($id)->code);

            $result = Bureau::destroy($id);

            return ($result) ? '1' : '0';
        }

        $this->validate($request,[
            'code' => 'required',
            'nom' => 'required',
        ]);

        $bureau = Bureau::findOrFail($id);
        $bureau->update($request->only('code','nom'));


        Action::addAction(session('userId'), 'bureaux', $bureau->idBureau, 'modifier',
        Etudiant::getEtudiantById(session('userId'))->nom.' a modifie le bureau: '.$bureau->code);

        return redirect()->back()->with('info',"Le bureau a été modifié avec succès");
    }
}
