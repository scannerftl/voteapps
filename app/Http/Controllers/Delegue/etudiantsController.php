<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 29/10/19
 * Time: 10:55
 */

namespace App\Http\Controllers\Delegue;

use App\Action;
use App\Classe;
use App\Etudiant;
use App\Http\Controllers\Controller;
use App\Http\Controllers\DelegueController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class etudiantsController extends DelegueController
{

    public function etudiantsList(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        $etudiants = Etudiant::listeDesEtudiantsEligible();

        return view('delegue.listeDesEtudiants',compact('etudiants'));
    }

    public function etudiantsListComplet(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        $etudiants = Etudiant::listeDesEtudiants();

        return view('delegue.listeDesEtudiantsComplet',compact('etudiants'));
    }

    public function etudiantsListUpdate(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        $etudiants = Etudiant::listeDesEtudiantsEligible();
        return view('ajax.listeDesEtudiants', compact('etudiants'));
    }

    public function ajouterEtudiant(){
        if(!$this->verifDelegue()) return redirect()->route('login');

        return view('delegue.ajouterEtudiant');
    }

    public function verifEtudiant(Request $request){

        $this->validate($request,[
            'nom' => 'required',
            'matricule' => 'required|unique:etudiants,matricule',
            'prenom' => 'required',
            'pseudo' => 'required|min:6|unique:etudiants,pseudo',
            'email' => 'required|email|unique:etudiants,email',
            'tel' => 'required|size:9|unique:etudiants,tel',
            'dateNaiss' => 'date',
            'idClasse' => 'required',
            'password' => 'min:6',
            'passwordConfirm' => 'required_with:password|same:password|min:6',
        ]);

        $etudiant = Etudiant::create($request->only('nom','prenom','pseudo','matricule','sexe','email','tel','dateNaiss','password','type','idClasse'));
        if($etudiant){
            //upload de la photo
            $photo = $request->file('image');
            if($photo){
                $path = 'source/user/images/'.Classe::getClasseById($request->idClasse)->code.'/';
                $fileName = uniqid('student-').'.'.$photo->extension();
                $name = $path.$fileName;
                $photo->move($path,$name);
                $etudiant->photo = $name;
                $etudiant->photoIbbLink = $this->getIBBLik($name);
            }

            //Enregistrement du mot de passe haché
            $etudiant->password = sha1($request->password);
            $etudiant->save();

            // Création des données pour l'envoi de mail
            $data[] = $request->all();
            $data[0]['photoIbbLink'] = $etudiant->photoIbbLink;
            $data = (object) $data[0];

            //Enregistrement des logs
            Action::addAction(
                session('userId'),
                'etudiants',
                $etudiant->idEtudiant, 'ajouter',
                Etudiant::getEtudiantById(session('userId'))->nom.' a cree '.$etudiant->nom
            );


            //Envoi du SMS
            //$this->sendMessage($data,'create');


            //Envoi du mail
//            $this->sendMail(
//                $etudiant->email,
//                'Inscription réussite!',
//                'mail.inscription',
//                $etudiant->nom,
//                $data
//            );


        }

        return redirect()->back()->with('info',"L'étudiant $etudiant->nom a été ajouté avec succès");
    }

    public function updateEtudiant(Request $request,$id){
        if(!$this->verifDelegue()) return redirect()->route('login');

        //activation de l'utilisateur
        if($request->idUpdate){
            $etudiant = Etudiant::findOrFail($id);

            ($etudiant->etat == 'actif') ? $etudiant->etat = 'inactif' : $etudiant->etat = 'actif';

            $etudiant->save();
            if($etudiant->etat == 'actif'){
                Action::addAction(session('userId'), 'etudiants', $etudiant->idEtudiant, 'activer',
                Etudiant::getEtudiantById(session('userId'))->nom.' a active '.$etudiant->nom);
            }else{
                Action::addAction(session('userId'), 'etudiants', $etudiant->idEtudiant, 'desactiver',
                Etudiant::getEtudiantById(session('userId'))->nom.' a desactive '.$etudiant->nom);
            }
            return $etudiant->etat;
        }

        //suppression de l'utilisateur
        if($request->idDelete){
            $etudiant = Etudiant::findOrFail($id);
            if($etudiant->visibility == 'visible'){
                $etudiant->visibility = 'hidden';
                $result = $etudiant->save();

                // Enregistrement des log
                Action::addAction(session('userId'), 'etudiants', $etudiant->idEtudiant, 'supprimer',
                    Etudiant::getEtudiantById(session('userId'))->nom.' a supprime '.$etudiant->nom);
            }else{
                $etudiant->visibility = 'visible';
                $result = $etudiant->save();

                // Enregistrement des log
                Action::addAction(session('userId'), 'etudiants', $etudiant->idEtudiant, 'restaurer',
                    Etudiant::getEtudiantById(session('userId'))->nom.' a restauré '.$etudiant->nom);
            }


            return $etudiant->visibility;
        }

        $this->validate($request,[
            'nom' => 'required',
            'prenom' => 'required',
            'pseudo' => 'required|min:6',
            'email' => 'required|email',
            'tel' => 'required|size:9',
            'dateNaiss' => 'date',
            'idClasse' => 'required',
        ]);

        $etudiant = Etudiant::findOrFail($id);
        if($etudiant){
            //upload de la photo
            $photo = $request->file('image');
            if($photo){
                $path = 'source/user/images/'.Classe::getClasseById($request->idClasse)->code.'/';
                $fileName = uniqid('student-').'.'.$photo->extension();
                $name = $path.$fileName;
                $photo->move($path,$name);
                $etudiant->photo = $name;
                $etudiant->photoIbbLink = $this->getIBBLik($name);
            }
            $etudiant->update($request->only('nom','prenom','pseudo','sexe','email','tel','dateNaiss','type','idClasse'));


            // Enregistrement des logs
            Action::addAction(
                session('userId'),
                'etudiants',
                $etudiant->idEtudiant,
                'modifier',
                Etudiant::getEtudiantById(session('userId'))->nom.' a modifie '.$etudiant->nom
            );
        }

        return redirect()->back()->with('info',"Les informations de l'étudiant ont bien été mis à jour");
    }

    public function afficherEtudiant($id){
        if(!$this->verifDelegue()) return redirect()->route('login');

        $std = Etudiant::getEtudiantById($id);
        return view('ajax.afficherEtudiant', compact('std'));
    }

    public function modifierEtudiant($id){
        if(!$this->verifDelegue()) return redirect()->route('login');
        $std = Etudiant::getEtudiantById($id);
        return view('ajax.modifierEtudiant', compact('std'));
    }

    public function genererCodeVote($id){
        $etudiant = Etudiant::findOrFail($id);
        if($etudiant->codeVote){
            return 0;
        } else {
            $etudiant->codeVote = "TBs-".mt_rand(1000,9999);
            if(Etudiant::where('codeVote', $etudiant->codeVote)->first()){
                $this->genererCodeVote($id);
            }
            $etudiant->save();


            Action::addAction(session('userId'), 'etudiants', $etudiant->idEtudiant, 'generer',
            Etudiant::getEtudiantById(session('userId'))->nom.' a genere le code de vote de '.$etudiant->nom);
            return $etudiant->codeVote;
        }
    }

}
