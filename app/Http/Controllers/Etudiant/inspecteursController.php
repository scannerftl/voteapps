<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 07/11/19
 * Time: 17:30
 */

namespace App\Http\Controllers\Etudiant;


use App\Candidat;
use App\Http\Controllers\EtudiantController;
use App\Inspecteur;
use App\Vote;

class inspecteursController extends EtudiantController
{
    public function inspecteurPage(){
        if(!$this->verifInspecteur()) return redirect()->route('login.vote');
        $ispt = Inspecteur::where('idEtudiant', session('userId'))->first()->idInspecteur;
        $infosVote = Vote::getInspectorResult($ispt);
        return view('controlVote', compact('infosVote'));
    }

    public function inspecteurPageUpdate(){
        if(!$this->verifInspecteur()) return redirect()->route('login.vote');
        $ispt = Inspecteur::where('idEtudiant', session('userId'))->first()->idInspecteur;
        $infosVote = Vote::getInspectorResult($ispt);

        return view('controlVoteUpdate', compact('infosVote'));
    }
    
}
