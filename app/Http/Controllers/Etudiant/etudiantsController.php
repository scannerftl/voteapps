<?php
/**
 * Created by PhpStorm.
 * User: wiltek
 * Date: 07/11/19
 * Time: 17:33
 */

namespace App\Http\Controllers\Etudiant;


use App\Bureau;
use App\Candidat;
use App\Etudiant;
use App\Http\Controllers\EtudiantController;
use App\Notification;
use App\Poste;
use App\Vote;

class etudiantsController extends EtudiantController
{
    public function vote(){
        if(!$this->posteExist()) return redirect()->route('login.poste');
        if(!$this->verifEtudiant()) return redirect()->route('login.vote');
        $candidats = Candidat::listeDesCandidats();
        return view('votePage', compact('candidats'));
    }

    public function afficherDetailsCandidat($id){
        $cdd = Candidat::getCandidatById($id);
        return view('detailCandidat', compact('cdd'));
    }

    public function voteCandidat($id){
        $verifVote = Vote::where('idEtudiant',session('userId'))->first();
        if(!$verifVote){
            $poste = session('idPoste');
            $etudiant = session('userId');
            $vote = Vote::create([
                'idPoste' => $poste,
                'idEtudiant' => $etudiant,
                'idCandidat' => $id
            ]);

            $data[0]['nom'] = Etudiant::find(session('userId'))->nom.'  '.Etudiant::find(session('userId'))->prenom;
            $data[0]['tel'] = Etudiant::find(session('userId'))->tel;
            $data[0]['codeVote'] = Etudiant::find(session('userId'))->codeVote;
            $data[0]['parti'] = Candidat::find($id)->partie;
            $data[0]['poste'] = Poste::find($poste)->code;
            $data[0]['bureau'] = Bureau::find(Poste::find($poste)->idBureau)->code;
            $data = (object)$data[0];
            //$this->sendMessage($data, 'vote');
            sleep(2);
            return $vote;
        } else {
            Notification::addNotification(
                'etudiants',
                session('userId'),
                'essaie de voter plus d\' une fois',
                3,
                Etudiant::find(session('userId'))->nom.' a essayé de voter deux fois sur le poste '.session('code').' du bureau '. Bureau::find(session('idBureau'))->code
            );
            return 0;
        }
    }
}