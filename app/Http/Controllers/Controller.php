<?php

namespace App\Http\Controllers;

use App\Bureau;
use App\Etudiant;
use App\Notification;
use App\Poste;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Twilio\Rest\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function verifAuthUser(){
        if(!session()->exists(['idEtudiant','email'])){
            return redirect()->route('loginUser');
        }
    }

    public function logUser(Etudiant $etudiant){
        session([
            'userId' => $etudiant->idEtudiant,
            'type' => $etudiant->type,
            'email' => $etudiant->email,
            'matricule' => $etudiant->matricule
        ]);
    }

    public function logPoste(Poste $poste){
        session([
            'idPoste' => $poste->idPoste,
            'code' => $poste->code,
            'idBureau' => $poste->idBureau
        ]);
    }


    public function unlogPoste(){
        session()->forget(['code','idBureau','idPoste']);
    }

    public function unlogUser(){
        session()->forget(['userId','type','email','matricule']);
    }


    public function userExist(){
        return session()->exists(['userId','type','email','matricule']);
    }

    public function posteExist(){
        return session()->exists(['code','idBureau','idPoste']);
    }

    public function verifierEssaiesDeConnexion(){
        if(!session()->has('essaies')){
            session(['essaies' => 0]);
        }
        session(['essaies' => session('essaies') +1]);

        if(session('essaies') >= 3){
            Notification::addNotification(
                'postes',
                session('idPoste'),
                'essaie multiple de connexion',
                2,
                'Un utilisateur a raté ses identifiats '.session('essaies').' fois sur le poste '.session('code').' du bureau '.Bureau::find(session('idBureau'))->code
            );
        }
    }

    public function annullerEssaiesDeConnexion(){
        session()->forget('essaies');
    }

    public function sendMessage($data, $type){

        $phone = '+237'.$data->tel;
        if($type == 'vote'){
            $message = view('sms.vote', compact('data'));
        }else{
            $message = view('sms.inscription',compact('data'));
        }
        // Find your Account Sid and Auth Token at twilio.com/console
        // DANGER! This is insecure. See http://twil.io/secure
        $sid    = env('API_TWILIO_SID');
        $token  = env('API_TWILIO_TOKEN');
        $twilio = new Client($sid, $token);

        $message = $twilio->messages
            ->create($phone, // to
                array(
                    "body" => $message,
                    "from" => "+18329814731"
                )
            );

        dump($message);
        die();
    }
}
