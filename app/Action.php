<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = 'actions';
    protected  $guarded = ['idAction'];
    public $timestamps = null;
    protected $primaryKey = 'idAction';
    protected $dates = ['dateCreation'];

    public static function addAction($idInitiator, $cible, $idCible, $type, $action){
        return static::create([
            'idEtudiant' => $idInitiator,
            'cible' => $cible,
            'idCible' => $idCible,
            'type' => $type,
            'action' => $action
        ]);
    }

    

}
