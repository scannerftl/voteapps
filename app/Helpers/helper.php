<?php

if (! function_exists('test')) {

    function verifAuth()
    {
        if(!session()->exists('idEtudiant')){
            return redirect()->route('loginAdmin');
        }
    }
}