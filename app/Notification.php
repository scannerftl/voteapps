<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';
    protected  $guarded = ['idNotification'];
    public $timestamps = null;
    protected $primaryKey = 'idNotification';
    protected $dates = ['dateCreation'];

    public static function addNotification($cible, $idCible, $type, $severite, $notification){
        return static::create([
            'cible' => $cible,
            'idCible' => $idCible,
            'type' => $type,
            'severite' => $severite,
            'notification' => $notification
        ]);
    }

}
