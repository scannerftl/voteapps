<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Inspecteur extends Model
{
    protected $table = 'inspecteurs';
    protected  $guarded = ['idInspecteur'];
    public $timestamps = null;
    protected $primaryKey = 'idInspecteur';
    protected $dates = ['dateCreation'];

    public static function listeDesInspecteurs(){
        return DB::select("SELECT inspecteurs.idInspecteur, classes.code as classe, bureaux.code as bCode, bureaux.nom as bureau, etudiants.idEtudiant, etudiants.email, etudiants.matricule, etudiants.nom, etudiants.tel, etudiants.prenom, etudiants.sexe, etudiants.photo, nomC, logo, partie
                            FROM inspecteurs
                            JOIN (select candidats.idCandidat, logo, partie, etudiants.nom as nomC from candidats inner join etudiants on candidats.idEtudiant=etudiants.idEtudiant)
                            as etudiantscandidats on inspecteurs.idCandidat=etudiantscandidats.idCandidat
                            JOIN etudiants ON inspecteurs.idEtudiant = etudiants.idEtudiant
                            JOIN bureaux ON inspecteurs.idBureau = bureaux.idBureau
                            JOIN classes ON etudiants.idClasse = classes.idClasse");
    }

    public static function getInspecteurById($id){
        return DB::select("SELECT inspecteurs.idInspecteur, classes.code as classe, bureaux.idBureau, bureaux.code as bCode, bureaux.nom as bureau,etudiants.idEtudiant, etudiants.email, etudiants.matricule, etudiants.nom, etudiants.type, etudiants.tel, etudiants.prenom, etudiants.sexe, etudiants.photo, etudiantscandidats.idCandidat, etudiantscandidats.nomC
                            FROM inspecteurs
                            JOIN (select candidats.idCandidat, etudiants.nom as nomC from candidats inner join etudiants on candidats.idEtudiant=etudiants.idEtudiant)
                            as etudiantscandidats on inspecteurs.idCandidat=etudiantscandidats.idCandidat
                            JOIN etudiants ON inspecteurs.idEtudiant = etudiants.idEtudiant
                            JOIN bureaux ON inspecteurs.idBureau = bureaux.idBureau
                            JOIN classes ON etudiants.idClasse = classes.idClasse
                            WHERE idInspecteur = ?", [$id])[0];
    }


}
