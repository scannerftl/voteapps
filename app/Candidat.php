<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Candidat extends Model
{
    protected $table = 'candidats';
    protected  $guarded = ['idCandidat'];
    public $timestamps = null;
    protected $primaryKey = 'idCandidat';
    protected $dates = ['dateCreation'];

    public static function listeDesCandidats(){
        return DB::select("SELECT classes.code as classe, etudiants.email, etudiants.matricule, etudiants.nom, etudiants.tel, etudiants.prenom, etudiants.sexe, etudiants.photo,candidats.idCandidat, candidats.logo, candidats.partie, candidats.slogan, candidats.description 
                                  FROM candidats
                                  JOIN etudiants ON candidats.idEtudiant = etudiants.idEtudiant
                                  JOIN classes ON etudiants.idClasse = classes.idClasse");
    }

    public static function getCandidatById($id){
        return DB::select("SELECT classes.code as classe, etudiants.email, etudiants.matricule, etudiants.nom, etudiants.tel, etudiants.prenom, etudiants.sexe, etudiants.photo,candidats.idCandidat, candidats.logo, candidats.partie, candidats.slogan, candidats.description 
                                  FROM candidats
                                  JOIN etudiants ON candidats.idEtudiant = etudiants.idEtudiant
                                  JOIN classes ON etudiants.idClasse = classes.idClasse
                                  WHERE idCandidat = ?", [$id])[0];
    }


}
