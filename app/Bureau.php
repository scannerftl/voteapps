<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bureau extends Model
{
    protected $table = 'bureaux';
    protected  $guarded = ['idBureau'];
    public $timestamps = null;
    protected $primaryKey = 'idBureau';
    protected $dates = ['dateCreation'];

    public static function listeDesBureaux(){
        return static::all();
    }

    public static function getBureauById($id){
        return static::find($id);
    }

}
