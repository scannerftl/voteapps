<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
        // Votes Routes

Route::get('/vote/control', [
    'as' => 'pageInspecteur',
    'uses' => 'Etudiant\inspecteursController@inspecteurPage'
]);

Route::get('/vote',[
    'as' => 'pageDeVote',
    'uses' => 'Etudiant\etudiantsController@vote'
]);

Route::post('/vote/candidat/detail/{id}',[
    'as' => 'detail.candidat',
    'uses' => 'Etudiant\etudiantsController@afficherDetailsCandidat'
]);

Route::post('/vote/candidat/{id}',[
   'as' => 'vote.candidat',
   'uses' => 'Etudiant\etudiantsController@voteCandidat'
]);

Route::post('/vote/control/update',[
   'as' => 'pageInspecteurUpdate',
   'uses' => 'Etudiant\inspecteursController@inspecteurPageUpdate'
]);




        // Login Routes

Route::get('/login',[
   'as' => 'login',
    'uses' => 'connexionController@login'
]);

Route::post('/login',[
    'as' => 'login.post',
    'uses' => 'connexionController@loginPost'
]);

Route::get('/vote/login',[
    'as' => 'login.vote',
    'uses' => 'connexionController@loginEtudiant'
]);

Route::post('/vote/login',[
    'as' => 'login.vote.post',
    'uses' => 'connexionController@loginEtudiantPost'
]);

Route::get('/poste/login',[
    'as' => 'login.poste',
    'uses' => 'connexionController@loginPoste'
]);

Route::post('/poste/login',[
    'as' => 'login.poste.post',
    'uses' => 'connexionController@loginPostePost'
]);

route::get('/logout/poste',[
    'as' => 'logout.poste',
    'uses' => 'connexionController@logoutPoste'
]);

Route::get('/logout',[
    'as' => 'logout',
    'uses' => 'connexionController@logout'
]);

Route::get('/logout/etudiant',[
    'as' => 'logout.etudiant',
    'uses' => 'connexionController@logoutUser'
]);


        // Etudiants Routes
Route::get('/',[
    'as' => 'delegue.dashboard',
    'uses' => 'Delegue\votesController@dashboard'
]);

Route::get('/etudiants/voters/list',[
    'as' => 'listedesetudiants',
    'uses' => 'Delegue\etudiantsController@etudiantsList'
]);

Route::get('/etudiants/list',[
    'as' => 'listedesetudiants.complet',
    'uses' => 'Delegue\etudiantsController@etudiantsListComplet'
]);

Route::post('/etudiants/list/update',[
    'as' => 'listedesetudiants.update',
    'uses' => 'Delegue\etudiantsController@etudiantsListUpdate'
]);

Route::get('/etudiants/ajouter',[
    'as' => 'ajouterEtudiant',
    'uses' => 'Delegue\etudiantsController@ajouterEtudiant'
]);

Route::post('/etudiants/ajouter',[
    'as' => 'ajouterEtudiantPost',
    'uses' => 'Delegue\etudiantsController@verifEtudiant'
]);

Route::post('/etudiants/modifier/{id}',[
    'as' => 'modifierEtudiant',
    'uses' => 'Delegue\etudiantsController@modifierEtudiant'
]);

Route::put('/etudiants/update/{id}',[
    'as' => 'updateEtudiant',
    'uses' => 'Delegue\etudiantsController@updateEtudiant'
]);

Route::post('/etudiants/afficher/{id}',[
    'as' => 'afficherEtudiant',
    'uses' => 'Delegue\etudiantsController@afficherEtudiant'
]);

Route::post('/etudiants/genererCode/{id}',[
    'as' => 'genererCodeVote',
    'uses' => 'Delegue\etudiantsController@genererCodeVote'
]);


// delegue management routes

Route::put('/notifications/read/{id}',[
    'as' => 'notification.read',
    'uses' => 'Delegue\notificationsController@markAsRead'
]);

Route::post('/notifications/list/update',[
    'as' => 'notification.list.update',
    'uses' => 'Delegue\notificationsController@updateNotificationsList'
]);

Route::get('/notifications',[
    'as' => 'notifications',
    'uses' => 'Delegue\notificationsController@notifications'
]);

  // Candidats routes

Route::get('/candidats/list',[
    'as' => 'listedescandidats',
    'uses' => 'Delegue\candidatsController@candidatList'
]);

Route::get('/candidats/ajouter',[
    'as' => 'ajoutercandidat',
    'uses' => 'Delegue\candidatsController@ajouterCandidat'
]);

Route::post('/candidats/ajouter',[
    'as' => 'ajouterCandidatPost',
    'uses' => 'Delegue\candidatsController@verifierCandidat'
]);

Route::post('/candidats/modifier/{id}',[
    'as' => 'modifiercandidat',
    'uses' => 'Delegue\candidatsController@modifierCandidat'
]);

Route::put('/candidats/update/{id}',[
    'as' => 'updateCandidat',
    'uses' => 'Delegue\candidatsController@updateCandidat'
]);

Route::post('/candidats/afficher/{id}',[
    'as' => 'afficherCandidat',
    'uses' => 'Delegue\candidatsController@afficherEtudiant'
]);

Route::post('/candidats/afficher/{id}',[
    'as' => 'afficherCandidat',
    'uses' => 'Delegue\candidatsController@afficherCandidat'
]);



// Inspecteurs routes

Route::get('/inspecteurs/list',[
    'as' => 'listedesinspecteurs',
    'uses' => 'Delegue\inspecteursController@inspecteurList'
]);

Route::get('/inspecteurs/ajouter',[
    'as' => 'ajouterinspecteur',
    'uses' => 'Delegue\inspecteursController@ajouterInspecteur'
]);

Route::post('/inspecteurs/ajouter',[
    'as' => 'ajouterInspecteurPost',
    'uses' => 'Delegue\inspecteursController@verifierInspecteur'
]);

Route::post('/inspecteurs/afficher/{id}',[
    'as' => 'afficherInspecteur',
    'uses' => 'Delegue\inspecteursController@afficherInspecteur'
]);


Route::post('/inspecteurs/modifier/{id}',[
    'as' => 'modifierinspecteur',
    'uses' => 'Delegue\inspecteursController@modifierInspecteur'
]);

Route::put('/inspecteurs/update/{id}',[
    'as' => 'updateInspecteur',
    'uses' => 'Delegue\inspecteursController@updateInspecteur'
]);






// Bureaux routes

Route::get('/bureaux/list',[
    'as' => 'listedesbureaux',
    'uses' => 'Delegue\bureauxController@bureauList'
]);

Route::get('/bureaux/ajouter',[
    'as' => 'ajouterbureau',
    'uses' => 'Delegue\bureauxController@ajouterBureau'
]);

Route::post('/bureaux/ajouter',[
    'as' => 'ajouterbureauPost',
    'uses' => 'Delegue\bureauxController@verifierBureau'
]);

Route::post('/bureaux/modifier/{id}',[
    'as' => 'modifierbureau',
    'uses' => 'Delegue\bureauxController@modifierBureau'
]);

Route::post('/bureaux/afficher/{id}',[
    'as' => 'afficherBureau',
    'uses' => 'Delegue\bureauxController@afficherBureau'
]);

Route::put('/bureaux/update/{id}',[
    'as' => 'updateBureau',
    'uses' => 'Delegue\bureauxController@updateBureau'
]);




// Postes routes

Route::get('/postes/list',[
    'as' => 'listedespostes',
    'uses' => 'Delegue\postesController@posteList'
]);

Route::get('/postes/ajouter',[
    'as' => 'ajouterposte',
    'uses' => 'Delegue\postesController@ajouterposte'
]);

Route::post('/postes/ajouter',[
    'as' => 'ajouterpostePost',
    'uses' => 'Delegue\postesController@verifierposte'
]);

Route::post('/postes/modifier/{id}',[
    'as' => 'modifierposte',
    'uses' => 'Delegue\postesController@modifierposte'
]);

Route::put('/postes/update/{id}',[
    'as' => 'updateposte',
    'uses' => 'Delegue\postesController@updateposte'
]);

Route::get('/test',function (){
    $etudiants = \App\Etudiant::all();
    return view('delegue.resultats', compact('etudiants'));
});

// Admin management route


// management pdf  route


Route::get('/inspecteurs/pdf-generetor1',[
    'as' => 'listeEtudiantsPdf',
    'uses' => 'Delegue\pdfGeneretor@listeEtudiantsPdf'
]);


Route::get('/inspecteurs/pdf-generetor2',[
    'as' => 'listeCandidatsPdf',
    'uses' => 'Delegue\pdfGeneretor@listeCandidatsPdf'
]);

Route::get('/inspecteurs/pdf-generetor_3',[
    'as' => 'statistiqueVotePdf',
    'uses' => 'Delegue\pdfGeneretor@statistiqueVotePdf'
]);



Route::get('/inspecteurs/pdf-generetor_4',[
    'as' => 'fichierLogPdf',
    'uses' => 'Delegue\pdfGeneretor@fichierLogPdf'
]);

Route::get('/inspecteurs/pdf-generetor3',[
    'as' => 'listeEtudiantsElecteuresPdf',
    'uses' => 'Delegue\pdfGeneretor@listeEtudiantsElecteuresPdf'
]);

Route::get('/inspecteurs/pdf-generetor4',[
    'as' => 'listeInspecteursPdf',
    'uses' => 'Delegue\pdfGeneretor@listeInspecteursPdf'
]);

Route::get('/inspecteurs/pdf-generetor5',[
    'as' => 'listePostesPdf',
    'uses' => 'Delegue\pdfGeneretor@listePostesPdf'
]);

Route::get('/inspecteurs/pdf-generetor6',[
    'as' => 'listeBureauxPdf',
    'uses' => 'Delegue\pdfGeneretor@listeBureauxPdf'
]);

Route::get('/inspecteurs/pdf-generetor7',[
    'as' => 'listeDeleguesPdf',
    'uses' => 'Delegue\pdfGeneretor@listeDeleguesPdf'
]);

Route::get('/inspecteurs/pdf-generetor8',[
    'as' => 'statistiqueResultatPdf',
    'uses' => 'Delegue\pdfGeneretor@statistiqueResultatPdf'
]);
